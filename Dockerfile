FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

WORKDIR /app/

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

# Copy poetry.lock* in case it doesn't exist in the repo
COPY pyproject.toml poetry.lock* /app/

# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"


RUN mkdir /app-libs
WORKDIR /app-libs/
RUN git clone https://github.com/MohamedAmineOuali/mongoengine.git
RUN git clone https://github.com/MohamedAmineOuali/fastapi-extra.git

#RUN mkdir -p /opt/project
#RUN ln -s /app-libs/mongoengine/mongoengine/ /opt/project/mongoengine

WORKDIR /app/
RUN ln -s /app-libs/mongoengine/mongoengine/ ./mongoengine
RUN ln -s /app-libs/fastapi-extra/fastapi_extra/ ./fastapi_extra


COPY ./app /app/app
ENV PYTHONPATH=/app
