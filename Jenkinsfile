pipeline {
  agent any
  options {
  skipDefaultCheckout()
  gitLabConnection('ipomm_gitlab')
  gitlabBuilds(builds: ["SCM", "Static Code Quality Analysis", "Tests", "Sonarqube Quality Analysis", "Deployment to Dev", "Deploy Production image to Docker Registry", "Deployment Production"])
  }
  triggers {
      gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
   }
  stages {
    stage('SCM') {
     steps {
        gitlabCommitStatus("SCM") {
          checkout scm
        }
     }
    }
    stage('Static Code Quality Analysis') {
      agent {
        docker {
          image 'ipomm/python-test-env'
          args '--network="devops"  -u "jenkins" -e PIP_CACHE_DIR=/var/jenkins_home/.cache/pip'
          // to use the same node and workdir defined on top-level pipeline for all docker agents
          reuseNode true
        }
      }
      steps {
            gitlabCommitStatus("Static Code Quality Analysis") {
            sh 'pip install -r requirements/requirements-code-quality.txt'
            sh 'invoke mypy'
            sh 'invoke black'
            sh 'invoke isort'
            sh 'invoke pylint'
            sh 'invoke flake8'
          }
      }
      post {
          always {
             // using warning next gen plugin
             recordIssues aggregatingResults: true, tools: [pyLint(pattern: '**/tests-reports/pylint-report.txt')]
             recordIssues aggregatingResults: true, tools: [flake8(pattern: '**/tests-reports/flake8.txt')]
          }
          success {
             stash(name: 'pylint-reports', includes: 'tests-reports/pylint-report.txt')
          }
      }
    }
    stage('Tests') {
      agent {
        docker {
          image 'ipomm/python-test-env'
          args '--network="devops" -u "jenkins" -e PIP_CACHE_DIR=/var/jenkins_home/.cache/pip'
          reuseNode true
        }
      }
      steps {
        gitlabCommitStatus("Tests") {
            sh 'poetry install'
            sh 'invoke test'
        }
      }
      post {
        always {
          junit 'tests-reports/junit.xml'
        }
        success {
             stash(name: 'tests-reports', includes: 'tests-reports/*.xml')
          }
      }
    }
    stage('Sonarqube Quality Analysis') {
      agent {
        docker {
          image 'ipomm/python-test-env'
          args '--network="devops" -u "jenkins"'
          reuseNode true
        }
      }
      steps {
       gitlabCommitStatus("Sonarqube Quality Analysis") {
           unstash 'tests-reports'
           unstash 'pylint-reports'
           sh 'sonar-scanner'
       }
      }
    }

    stage('Deployment to Dev') {
      when {
         branch 'develop'
      }
      steps {
        gitlabCommitStatus("Deployment to Dev") {
            // Remove possibly previous broken stacks left hanging after an error
            sh 'docker-compose -f docker-compose.yml -f docker-compose.prod.yml down --remove-orphans'
            sh 'docker-compose -f docker-compose.yml -f docker-compose.prod.yml build'
            sh 'docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --force-recreate'
        }
      }
    }

    stage('Deploy Production image to Docker Registry') {
      when {
         branch 'master'
      }
      steps {
        gitlabCommitStatus("Deploy Production image to Docker Registry") {
            sh 'TAG=prod docker-compose -f docker-compose.yml -f  docker-compose.prod.yml push'
        }
      }
    }
    stage('Deployment to Production') {
      when {
         branch 'master'
      }
      steps {
         gitlabCommitStatus("Deployment to Production") {
            echo 'Deploying to K8S....'
         }
      }
    }

  }
}