# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

import os
import sys

from datetime import datetime

from invoke import task, call

ROOT = os.path.dirname(__file__)

CLEAN_PATTERNS = [
    "build",
    "dist",
    "cover",
    "docs/_build",
    "**/*.pyc",
    ".tox",
    "**/__pycache__",
    "*.egg-info",
    ".benchmarks",
    ".pytest_cache",
    "app.egg-info",
    ".mypy_cache",
    ".coverage",
    "tests-reports",
    "flake8.txt"
]


def color(code):
    """Format output by adding colors to output."""
    return lambda t: "\033[{0}{1}\033[0;m".format(code, t)


green = color("1;32m")
red = color("1;31m")
blue = color("1;30m")
cyan = color("1;36m")
purple = color("1;35m")
white = color("1;39m")


def header(text):
    """Display an header."""
    print(" ".join((blue(">>"), cyan(text))))
    sys.stdout.flush()


def info(text, *args, **kwargs):
    """Display informations."""
    text = text.format(*args, **kwargs)
    print(" ".join((purple(">>>"), text)))
    sys.stdout.flush()


def success(text):
    """Display a success message."""
    print(" ".join((green(">>"), white(text))))
    sys.stdout.flush()


def error(text):
    """Display an error message."""
    print(red("✘ {0}".format(text)))
    sys.stdout.flush()


def build_args(*args):
    return " ".join(a for a in args if a)


@task
def clean(ctx):
    """Cleanup all build artifacts."""
    header(clean.__doc__)
    with ctx.cd(ROOT):
        for pattern in CLEAN_PATTERNS:
            info("Removing {0}", pattern)
            ctx.run("rm -rf {0}".format(pattern))


@task(optional=['install', 'update'])
def dependency(ctx, show=False, install=None, update=None, remove=None, dev=False):
    """Install dependencies"""
    header(dependency.__doc__)

    args = [install, remove, update, show]

    nbArgs = 0
    for arg in args:
        if arg:
            nbArgs += 1

    if nbArgs != 1:
        error("number of arguments is incorrect")
        return

    command = ""
    if install:
        if install is True:
            command = "poetry install "
            if not dev:
                command += "--no-dev"
        else:
            command = "poetry add " + install + " "
            if dev:
                command += "--dev"
    elif update:
        if update is True:
            command = "poetry update "
            if not dev:
                command += "--no-dev"
        else:
            command = "poetry update " + update + " "
            if dev:
                command += "--dev"
    elif remove:
        if remove is True:
            error("specify what to remove")
        else:
            command = "poetry remove " + remove + " "
            if dev:
                command += "--dev"
    elif show:
        command = "poetry show "
        if not dev:
            command += "--no-dev"

    info(command)

    with ctx.cd(ROOT):
        ctx.run(
            command, pty=True
        )


@task
def test(ctx, profile=False):
    """Run tests suite."""
    header(test.__doc__)
    with ctx.cd(ROOT):
        ctx.run(
            "pytest --cov=app --cov-report=term-missing --cov-report=xml:tests-reports/coverage.xml"
            " -o junit_family=xunit2 --junit-xml=tests-reports/junit.xml tests"
            , pty=True
        )


@task
def benchmark(
        ctx,
        max_time=2,
        save=False,
        compare=False,
        histogram=False,
        profile=False,
        tox=False):
    """Run benchmarks."""
    header(benchmark.__doc__)
    ts = datetime.now()
    kwargs = build_args(
        "--benchmark-max-time={0}".format(max_time),
        "--benchmark-autosave" if save else None,
        "--benchmark-compare" if compare else None,
        "--benchmark-histogram=histograms/{0:%Y%m%d-%H%M%S}".format(ts)
        if histogram else None,
        "--benchmark-cprofile=tottime" if profile else None,
    )
    cmd = "pytest tests/benchmarks {0}".format(kwargs)
    if tox:
        envs = ctx.run("tox -l", hide=True).stdout.splitlines()
        envs = ",".join(e for e in envs if e != "doc")
        cmd = "tox -e {envs} -- {cmd}".format(envs=envs, cmd=cmd)
    ctx.run(cmd, pty=True)


@task
def coverage(ctx, html=False):
    """Run tests suite with coverage."""
    header(coverage.__doc__)
    extra = "--cov-report html:tests-reports/coverage.html" if html else ""
    with ctx.cd(ROOT):
        ctx.run(
            "pytest --cov app --cov-report term --cov-report xml:tests-reports/coverage.xml {0}".format(
                extra
            ),
            pty=True,
        )


@task
def format(ctx):
    """Run a quality format"""
    header(format.__doc__)
    with ctx.cd(ROOT):
        # Sort imports one per line, so autoflake can remove unused imports
        ctx.run("isort --recursive --apply app", pty=True)
        ctx.run("autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place app "
                "--exclude=__init__.py", pty=True)
        ctx.run("black app", pty=True)


@task
def mypy(ctx):
    """Run a mypy static type checker for Python."""
    header(mypy.__doc__)
    with ctx.cd(ROOT):
        ctx.run(" mypy app  --follow-imports silent || true", pty=True)


@task
def black(ctx):
    """Run a black Code formatter."""
    header(black.__doc__)
    with ctx.cd(ROOT):
        ctx.run("black app --check", pty=True)


@task
def isort(ctx, called=True):
    """Run a isort Import formatter."""
    header(isort.__doc__)
    with ctx.cd(ROOT):
        if called:
            ctx.run("isort --check-only --recursive app || true", pty=True)
        else:
            ctx.run("isort --check-only --recursive app", pty=True)


@task
def createReportsDir(ctx):
    """Create tests-reports directory if it doesn't exist."""
    header(createReportsDir.__doc__)
    with ctx.cd(ROOT):
        ctx.run("[ -d tests-reports ] || mkdir tests-reports", pty=True)


@task(pre=[call(createReportsDir)])
def flake8(ctx, called=True):
    """Run a flake8 report."""
    header(flake8.__doc__)
    with ctx.cd(ROOT):
        if called:
            ctx.run("flake8 --output-file=tests-reports/flake8.txt || true", pty=True)
        else:
            ctx.run("flake8 --output-file=tests-reports/flake8.txt", pty=True)


@task(pre=[call(createReportsDir)])
def pylint(ctx, called=True, report=False):
    """Run a pylint report."""
    header(pylint.__doc__)
    extra = "-f parseable -r y" if report else "-f parseable"
    with ctx.cd(ROOT):
        if called:
            ctx.run("pylint {0} app/ > tests-reports/pylint-report.txt || true".format(extra), pty=True)
        else:
            ctx.run("pylint {0} app/ > tests-reports/pylint-report.txt".format(extra), pty=True)


@task(pre=[call(pylint, called=True)])
def sonarqube(ctx):
    """Run SonarScanner..."""
    header(sonarqube.__doc__)
    with ctx.cd(ROOT):
        ctx.run("sonar-scanner", pty=True)


@task(pre=[call(mypy, called=True),
           call(black, called=True),
           call(isort, called=True),
           call(pylint, called=True),
           call(flake8, called=True)])
def qa(ctx):
    """Run a quality report."""
    pass


@task
def doc(ctx):
    """Build the documentation."""
    header(doc.__doc__)
    with ctx.cd(os.path.join(ROOT, "doc")):
        ctx.run("make html", pty=True)


@task
def build(ctx):
    """Create a package for distribution."""
    header(build.__doc__)
    with ctx.cd(ROOT):
        ctx.run("poetry build", pty=True)


@task(clean, test, doc, qa, build, default=True)
def all(ctx):
    """Run tests, reports and packaging."""
    pass

# invoke
