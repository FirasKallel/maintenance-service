from typing import Union

from pydantic import BaseModel

from app.models import ifc_models
from app.models.ifc_models.schemas.ifc2x3.models.core.ifckernel.entities.ifc_actor import IfcActor
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_address import IfcAddress
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_organization import \
    IfcOrganization
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_person import IfcPerson
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_person_and_organization import \
    IfcPersonAndOrganization
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_postal_address import \
    IfcPostalAddress
from app.models.ifc_models.schemas.ifc2x3.models.resource.ifcactorresource.entities.ifc_telecom_address import \
    IfcTelecomAddress
from app.repository.base import Repository


class Unset(BaseModel):
    def dict(
            self,
            *,
            include: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
            exclude: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
            by_alias: bool = False,
            skip_defaults: bool = None,
            exclude_unset: bool = False,
            exclude_defaults: bool = False,
            exclude_none: bool = False,
    ) -> 'DictStrAny':
        return ifc_models.Unset()


class IfcActorRepository(Repository):
    pass


ifcPersonRepository = IfcActorRepository(IfcPerson)
ifcOrganizationRepository = IfcActorRepository(IfcOrganization)
ifcPostalAddressRepository = IfcActorRepository(IfcPostalAddress)
ifcTelecomAddressRepository = IfcActorRepository(IfcTelecomAddress)
ifcPersonAndOrganizationRepository = IfcActorRepository(IfcPersonAndOrganization)
ifcActorRepository = IfcActorRepository(IfcActor)
ifcAddressRepository=IfcActorRepository(IfcAddress)
