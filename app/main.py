from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.gzip import GZipMiddleware

from app.api.default.api import api_router
from app.utils.config import settings
from fastapi_extra.middleware.tenant_middleware import enable_tenant_middleware
from fastapi_extra.utils.core.startup_shutdown_events import shutdown, startup

app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_DEFAULT_STR}/openapi.json",
    docs_url=f"{settings.API_DEFAULT_STR}/docs",
    redoc_url=f"{settings.API_DEFAULT_STR}/redoc",
    on_startup=startup,
    on_shutdown=shutdown,
)

# Set all CORS enabled origins
app.add_middleware(GZipMiddleware, minimum_size=1000)

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
enable_tenant_middleware(app, with_db=True)


app.include_router(api_router, prefix=settings.API_DEFAULT_STR)
