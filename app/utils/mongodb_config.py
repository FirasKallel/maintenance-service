from app.utils.config import settings
from mongoengine import connect

connect(
    settings.MONGODB_DEFAULT_DB,
    host=settings.MONGODB_SERVER,
    username=settings.MONGODB_USER,
    password=settings.MONGODB_PASSWORD,
    authentication_source="admin",
)
