from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_element_type import IfcElementType


class IfcElementComponentType(IfcElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

