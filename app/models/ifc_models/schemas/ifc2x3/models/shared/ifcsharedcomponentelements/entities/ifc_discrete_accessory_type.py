from mongoengine import DynamicField
from .ifc_element_component_type import IfcElementComponentType


class IfcDiscreteAccessoryType(IfcElementComponentType):
    meta = {
        'namespace': 'ifc2x3'
    }

