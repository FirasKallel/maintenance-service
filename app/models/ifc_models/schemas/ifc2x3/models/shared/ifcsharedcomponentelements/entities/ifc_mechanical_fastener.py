from mongoengine import DynamicField
from .ifc_fastener import IfcFastener


class IfcMechanicalFastener(IfcFastener):
    meta = {
        'namespace': 'ifc2x3'
    }

    NominalDiameter = DynamicField()
    NominalLength = DynamicField()
