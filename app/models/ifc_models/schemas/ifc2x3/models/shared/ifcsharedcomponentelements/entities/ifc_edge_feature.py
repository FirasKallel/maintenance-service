from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_feature_element_subtraction import IfcFeatureElementSubtraction


class IfcEdgeFeature(IfcFeatureElementSubtraction):
    meta = {
        'namespace': 'ifc2x3'
    }

    FeatureLength = DynamicField()
