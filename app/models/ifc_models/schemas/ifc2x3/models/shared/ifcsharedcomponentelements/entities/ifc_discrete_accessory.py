from mongoengine import DynamicField
from .ifc_element_component import IfcElementComponent


class IfcDiscreteAccessory(IfcElementComponent):
    meta = {
        'namespace': 'ifc2x3'
    }

