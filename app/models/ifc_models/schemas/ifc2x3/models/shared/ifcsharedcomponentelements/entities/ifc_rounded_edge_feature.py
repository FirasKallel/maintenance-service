from mongoengine import DynamicField
from .ifc_edge_feature import IfcEdgeFeature


class IfcRoundedEdgeFeature(IfcEdgeFeature):
    meta = {
        'namespace': 'ifc2x3'
    }

    Radius = DynamicField()
