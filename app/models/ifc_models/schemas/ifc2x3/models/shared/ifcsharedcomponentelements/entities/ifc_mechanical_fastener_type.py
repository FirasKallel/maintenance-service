from mongoengine import DynamicField
from .ifc_fastener_type import IfcFastenerType


class IfcMechanicalFastenerType(IfcFastenerType):
    meta = {
        'namespace': 'ifc2x3'
    }

