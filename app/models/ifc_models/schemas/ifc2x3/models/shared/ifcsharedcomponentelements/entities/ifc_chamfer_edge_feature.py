from mongoengine import DynamicField
from .ifc_edge_feature import IfcEdgeFeature


class IfcChamferEdgeFeature(IfcEdgeFeature):
    meta = {
        'namespace': 'ifc2x3'
    }

    Width = DynamicField()
    Height = DynamicField()
