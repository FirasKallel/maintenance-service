from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_element import IfcElement


class IfcElementComponent(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

