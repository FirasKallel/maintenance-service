from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcWindow(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    OverallHeight = DynamicField()
    OverallWidth = DynamicField()
