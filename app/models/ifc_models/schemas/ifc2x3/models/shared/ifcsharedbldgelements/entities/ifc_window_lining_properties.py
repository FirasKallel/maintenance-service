from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcWindowLiningProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    LiningDepth = DynamicField()
    LiningThickness = DynamicField()
    TransomThickness = DynamicField()
    MullionThickness = DynamicField()
    FirstTransomOffset = DynamicField()
    SecondTransomOffset = DynamicField()
    FirstMullionOffset = DynamicField()
    SecondMullionOffset = DynamicField()
    ShapeAspectStyle = DynamicField()
