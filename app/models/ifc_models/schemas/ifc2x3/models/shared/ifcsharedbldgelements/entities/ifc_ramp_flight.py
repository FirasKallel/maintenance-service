from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcRampFlight(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

