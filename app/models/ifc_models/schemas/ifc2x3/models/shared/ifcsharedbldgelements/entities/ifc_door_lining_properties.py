from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcDoorLiningProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    LiningDepth = DynamicField()
    LiningThickness = DynamicField()
    ThresholdDepth = DynamicField()
    ThresholdThickness = DynamicField()
    TransomThickness = DynamicField()
    TransomOffset = DynamicField()
    LiningOffset = DynamicField()
    ThresholdOffset = DynamicField()
    CasingThickness = DynamicField()
    CasingDepth = DynamicField()
    ShapeAspectStyle = DynamicField()
