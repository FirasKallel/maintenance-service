from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcDoorPanelProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    PanelDepth = DynamicField()
    PanelOperation = DynamicField()
    PanelWidth = DynamicField()
    PanelPosition = DynamicField()
    ShapeAspectStyle = DynamicField()
