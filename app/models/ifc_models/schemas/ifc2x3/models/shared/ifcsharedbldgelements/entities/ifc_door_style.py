from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_type_product import IfcTypeProduct


class IfcDoorStyle(IfcTypeProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    OperationType = DynamicField()
    ConstructionType = DynamicField()
    ParameterTakesPrecedence = DynamicField()
    Sizeable = DynamicField()
