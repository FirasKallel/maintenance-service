from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcSlab(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
