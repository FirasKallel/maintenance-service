from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcBeam(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

