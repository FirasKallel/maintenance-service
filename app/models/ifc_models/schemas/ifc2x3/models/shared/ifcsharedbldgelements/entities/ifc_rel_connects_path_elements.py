from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_rel_connects_elements import IfcRelConnectsElements


class IfcRelConnectsPathElements(IfcRelConnectsElements):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingPriorities = DynamicField()
    RelatedPriorities = DynamicField()
    RelatedConnectionType = DynamicField()
    RelatingConnectionType = DynamicField()
