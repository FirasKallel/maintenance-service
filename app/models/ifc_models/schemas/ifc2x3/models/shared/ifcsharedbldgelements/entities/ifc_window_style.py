from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_type_product import IfcTypeProduct


class IfcWindowStyle(IfcTypeProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    ConstructionType = DynamicField()
    OperationType = DynamicField()
    ParameterTakesPrecedence = DynamicField()
    Sizeable = DynamicField()
