from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element_type import IfcBuildingElementType


class IfcWallType(IfcBuildingElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
