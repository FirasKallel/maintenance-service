from mongoengine import DynamicField
from .ifc_wall import IfcWall


class IfcWallStandardCase(IfcWall):
    meta = {
        'namespace': 'ifc2x3'
    }

