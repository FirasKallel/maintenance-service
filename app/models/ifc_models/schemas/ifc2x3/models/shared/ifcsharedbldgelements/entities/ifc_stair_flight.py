from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcStairFlight(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    NumberOfRiser = DynamicField()
    NumberOfTreads = DynamicField()
    RiserHeight = DynamicField()
    TreadLength = DynamicField()
