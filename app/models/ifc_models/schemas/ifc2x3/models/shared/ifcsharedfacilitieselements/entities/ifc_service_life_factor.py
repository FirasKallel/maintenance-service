from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcServiceLifeFactor(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
    UpperValue = DynamicField()
    MostUsedValue = DynamicField()
    LowerValue = DynamicField()
