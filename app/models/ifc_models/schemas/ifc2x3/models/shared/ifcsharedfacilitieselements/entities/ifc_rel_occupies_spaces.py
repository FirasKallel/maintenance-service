from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_assigns_to_actor import IfcRelAssignsToActor


class IfcRelOccupiesSpaces(IfcRelAssignsToActor):
    meta = {
        'namespace': 'ifc2x3'
    }

