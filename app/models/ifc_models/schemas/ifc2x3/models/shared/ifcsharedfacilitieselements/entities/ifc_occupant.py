from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_actor import IfcActor


class IfcOccupant(IfcActor):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
