from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_group import IfcGroup


class IfcAsset(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

    AssetID = DynamicField()
    OriginalValue = DynamicField()
    CurrentValue = DynamicField()
    TotalReplacementCost = DynamicField()
    Owner = DynamicField()
    User = DynamicField()
    ResponsiblePerson = DynamicField()
    IncorporationDate = DynamicField()
    DepreciatedValue = DynamicField()
