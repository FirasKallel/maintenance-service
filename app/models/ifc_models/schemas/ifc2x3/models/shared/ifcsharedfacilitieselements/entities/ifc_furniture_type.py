from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_furnishing_element_type import IfcFurnishingElementType


class IfcFurnitureType(IfcFurnishingElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    AssemblyPlace = DynamicField()
