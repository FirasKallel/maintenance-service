from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_group import IfcGroup


class IfcInventory(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

    InventoryType = DynamicField()
    Jurisdiction = DynamicField()
    ResponsiblePersons = DynamicField()
    LastUpdateDate = DynamicField()
    CurrentValue = DynamicField()
    OriginalValue = DynamicField()
