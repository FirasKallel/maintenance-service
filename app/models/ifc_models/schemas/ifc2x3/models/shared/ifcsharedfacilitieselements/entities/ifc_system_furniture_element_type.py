from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_furnishing_element_type import IfcFurnishingElementType


class IfcSystemFurnitureElementType(IfcFurnishingElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

