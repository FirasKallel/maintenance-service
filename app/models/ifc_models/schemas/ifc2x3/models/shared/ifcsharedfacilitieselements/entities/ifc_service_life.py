from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcServiceLife(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    ServiceLifeType = DynamicField()
    ServiceLifeDuration = DynamicField()
