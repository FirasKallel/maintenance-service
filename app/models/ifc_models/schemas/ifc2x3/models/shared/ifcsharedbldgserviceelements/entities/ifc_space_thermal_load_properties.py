from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcSpaceThermalLoadProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    ApplicableValueRatio = DynamicField()
    ThermalLoadSource = DynamicField()
    PropertySource = DynamicField()
    SourceDescription = DynamicField()
    MaximumValue = DynamicField()
    MinimumValue = DynamicField()
    ThermalLoadTimeSeriesValues = DynamicField()
    UserDefinedThermalLoadSource = DynamicField()
    UserDefinedPropertySource = DynamicField()
    ThermalLoadType = DynamicField()
