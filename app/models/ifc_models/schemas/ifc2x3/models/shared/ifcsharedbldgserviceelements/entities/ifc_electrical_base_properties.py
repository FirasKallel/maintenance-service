from mongoengine import DynamicField
from .ifc_energy_properties import IfcEnergyProperties


class IfcElectricalBaseProperties(IfcEnergyProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    ElectricCurrentType = DynamicField()
    InputVoltage = DynamicField()
    InputFrequency = DynamicField()
    FullLoadCurrent = DynamicField()
    MinimumCircuitCurrent = DynamicField()
    MaximumPowerInput = DynamicField()
    RatedPowerInput = DynamicField()
    InputPhase = DynamicField()
