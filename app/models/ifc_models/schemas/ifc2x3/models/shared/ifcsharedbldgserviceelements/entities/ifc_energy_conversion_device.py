from mongoengine import DynamicField
from .ifc_distribution_flow_element import IfcDistributionFlowElement


class IfcEnergyConversionDevice(IfcDistributionFlowElement):
    meta = {
        'namespace': 'ifc2x3'
    }

