from mongoengine import DynamicField
from .ifc_distribution_flow_element_type import IfcDistributionFlowElementType


class IfcDistributionChamberElementType(IfcDistributionFlowElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
