from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcEnergyProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    EnergySequence = DynamicField()
    UserDefinedEnergySequence = DynamicField()
