from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_port import IfcPort


class IfcDistributionPort(IfcPort):
    meta = {
        'namespace': 'ifc2x3'
    }

    FlowDirection = DynamicField()
