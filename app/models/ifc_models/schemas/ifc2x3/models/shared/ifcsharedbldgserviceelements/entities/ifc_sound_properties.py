from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcSoundProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    IsAttenuating = DynamicField()
    SoundScale = DynamicField()
    SoundValues = DynamicField()
