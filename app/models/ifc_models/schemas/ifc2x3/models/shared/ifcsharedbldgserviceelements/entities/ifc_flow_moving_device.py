from mongoengine import DynamicField
from .ifc_distribution_flow_element import IfcDistributionFlowElement


class IfcFlowMovingDevice(IfcDistributionFlowElement):
    meta = {
        'namespace': 'ifc2x3'
    }

