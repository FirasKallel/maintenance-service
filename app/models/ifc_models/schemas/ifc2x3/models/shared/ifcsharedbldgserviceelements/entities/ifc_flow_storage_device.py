from mongoengine import DynamicField
from .ifc_distribution_flow_element import IfcDistributionFlowElement


class IfcFlowStorageDevice(IfcDistributionFlowElement):
    meta = {
        'namespace': 'ifc2x3'
    }

