from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcSoundValue(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    SoundLevelTimeSeries = DynamicField()
    Frequency = DynamicField()
    SoundLevelSingleValue = DynamicField()
