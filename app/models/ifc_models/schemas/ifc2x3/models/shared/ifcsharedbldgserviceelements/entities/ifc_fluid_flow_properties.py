from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcFluidFlowProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    PropertySource = DynamicField()
    FlowConditionTimeSeries = DynamicField()
    VelocityTimeSeries = DynamicField()
    FlowrateTimeSeries = DynamicField()
    Fluid = DynamicField()
    PressureTimeSeries = DynamicField()
    UserDefinedPropertySource = DynamicField()
    TemperatureSingleValue = DynamicField()
    WetBulbTemperatureSingleValue = DynamicField()
    WetBulbTemperatureTimeSeries = DynamicField()
    TemperatureTimeSeries = DynamicField()
    FlowrateSingleValue = DynamicField()
    FlowConditionSingleValue = DynamicField()
    VelocitySingleValue = DynamicField()
    PressureSingleValue = DynamicField()
