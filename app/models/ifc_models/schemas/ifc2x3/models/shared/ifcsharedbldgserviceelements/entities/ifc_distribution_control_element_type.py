from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_distribution_element_type import IfcDistributionElementType


class IfcDistributionControlElementType(IfcDistributionElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

