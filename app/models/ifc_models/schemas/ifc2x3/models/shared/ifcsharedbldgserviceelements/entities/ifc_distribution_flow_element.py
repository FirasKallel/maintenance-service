from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_distribution_element import IfcDistributionElement


class IfcDistributionFlowElement(IfcDistributionElement):
    meta = {
        'namespace': 'ifc2x3'
    }

