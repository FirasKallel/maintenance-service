from mongoengine import DynamicField
from .ifc_distribution_flow_element import IfcDistributionFlowElement


class IfcFlowSegment(IfcDistributionFlowElement):
    meta = {
        'namespace': 'ifc2x3'
    }

