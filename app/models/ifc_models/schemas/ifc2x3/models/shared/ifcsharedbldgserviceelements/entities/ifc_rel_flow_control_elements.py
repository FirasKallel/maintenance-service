from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelFlowControlElements(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatedControlElements = DynamicField()
    RelatingFlowElement = DynamicField()
