from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesAppliedValue(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingAppliedValue = DynamicField()
