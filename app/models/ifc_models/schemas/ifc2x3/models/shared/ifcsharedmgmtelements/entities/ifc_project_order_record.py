from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcProjectOrderRecord(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    Records = DynamicField()
    PredefinedType = DynamicField()
