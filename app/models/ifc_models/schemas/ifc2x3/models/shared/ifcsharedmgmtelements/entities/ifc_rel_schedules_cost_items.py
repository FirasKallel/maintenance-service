from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_assigns_to_control import IfcRelAssignsToControl


class IfcRelSchedulesCostItems(IfcRelAssignsToControl):
    meta = {
        'namespace': 'ifc2x3'
    }

