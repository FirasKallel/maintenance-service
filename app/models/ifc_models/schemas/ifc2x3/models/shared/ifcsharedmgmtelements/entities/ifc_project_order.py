from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcProjectOrder(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    ID = DynamicField()
    PredefinedType = DynamicField()
    Status = DynamicField()
