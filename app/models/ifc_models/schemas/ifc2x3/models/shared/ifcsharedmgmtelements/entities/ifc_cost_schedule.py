from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcCostSchedule(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    SubmittedBy = DynamicField()
    PreparedBy = DynamicField()
    SubmittedOn = DynamicField()
    Status = DynamicField()
    TargetUsers = DynamicField()
    UpdateDate = DynamicField()
    ID = DynamicField()
    PredefinedType = DynamicField()
