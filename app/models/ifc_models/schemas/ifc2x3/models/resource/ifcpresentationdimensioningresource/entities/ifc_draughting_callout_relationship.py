from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDraughtingCalloutRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    RelatingDraughtingCallout = DynamicField()
    RelatedDraughtingCallout = DynamicField()
