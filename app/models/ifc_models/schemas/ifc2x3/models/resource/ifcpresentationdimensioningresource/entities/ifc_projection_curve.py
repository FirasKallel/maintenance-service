from mongoengine import DynamicField
from ...ifcpresentationdefinitionresource.entities.ifc_annotation_curve_occurrence import IfcAnnotationCurveOccurrence


class IfcProjectionCurve(IfcAnnotationCurveOccurrence):
    meta = {
        'namespace': 'ifc2x3'
    }

