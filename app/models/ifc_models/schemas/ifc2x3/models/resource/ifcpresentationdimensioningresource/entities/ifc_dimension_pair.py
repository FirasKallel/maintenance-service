from mongoengine import DynamicField
from .ifc_draughting_callout_relationship import IfcDraughtingCalloutRelationship


class IfcDimensionPair(IfcDraughtingCalloutRelationship):
    meta = {
        'namespace': 'ifc2x3'
    }

