from mongoengine import DynamicField
from .ifc_draughting_callout import IfcDraughtingCallout


class IfcDimensionCurveDirectedCallout(IfcDraughtingCallout):
    meta = {
        'namespace': 'ifc2x3'
    }

