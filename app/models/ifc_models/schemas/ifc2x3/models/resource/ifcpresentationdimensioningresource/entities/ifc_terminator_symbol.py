from mongoengine import DynamicField
from ...ifcpresentationdefinitionresource.entities.ifc_annotation_symbol_occurrence import IfcAnnotationSymbolOccurrence


class IfcTerminatorSymbol(IfcAnnotationSymbolOccurrence):
    meta = {
        'namespace': 'ifc2x3'
    }

    AnnotatedCurve = DynamicField()
