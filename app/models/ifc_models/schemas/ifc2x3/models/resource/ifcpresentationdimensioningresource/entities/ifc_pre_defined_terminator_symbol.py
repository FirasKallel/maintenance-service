from mongoengine import DynamicField
from ...ifcpresentationdefinitionresource.entities.ifc_pre_defined_symbol import IfcPreDefinedSymbol


class IfcPreDefinedTerminatorSymbol(IfcPreDefinedSymbol):
    meta = {
        'namespace': 'ifc2x3'
    }

