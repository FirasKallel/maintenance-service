from mongoengine import DynamicField
from .ifc_dimension_curve_directed_callout import IfcDimensionCurveDirectedCallout


class IfcLinearDimension(IfcDimensionCurveDirectedCallout):
    meta = {
        'namespace': 'ifc2x3'
    }

