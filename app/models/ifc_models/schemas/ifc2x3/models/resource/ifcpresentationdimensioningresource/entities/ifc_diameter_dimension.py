from mongoengine import DynamicField
from .ifc_dimension_curve_directed_callout import IfcDimensionCurveDirectedCallout


class IfcDiameterDimension(IfcDimensionCurveDirectedCallout):
    meta = {
        'namespace': 'ifc2x3'
    }

