from mongoengine import DynamicField
from .ifc_terminator_symbol import IfcTerminatorSymbol


class IfcDimensionCurveTerminator(IfcTerminatorSymbol):
    meta = {
        'namespace': 'ifc2x3'
    }

    Role = DynamicField()
