from mongoengine import DynamicField
from .ifc_topological_representation_item import IfcTopologicalRepresentationItem


class IfcConnectedFaceSet(IfcTopologicalRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    CfsFaces = DynamicField()
