from mongoengine import DynamicField
from .ifc_vertex import IfcVertex


class IfcVertexPoint(IfcVertex):
    meta = {
        'namespace': 'ifc2x3'
    }

    VertexGeometry = DynamicField()
