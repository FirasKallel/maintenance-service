from mongoengine import DynamicField
from .ifc_loop import IfcLoop


class IfcEdgeLoop(IfcLoop):
    meta = {
        'namespace': 'ifc2x3'
    }

    EdgeList = DynamicField()
