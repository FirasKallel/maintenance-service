from mongoengine import DynamicField
from .ifc_edge import IfcEdge


class IfcSubedge(IfcEdge):
    meta = {
        'namespace': 'ifc2x3'
    }

    ParentEdge = DynamicField()
