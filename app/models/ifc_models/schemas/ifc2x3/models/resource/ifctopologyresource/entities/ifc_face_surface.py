from mongoengine import DynamicField
from .ifc_face import IfcFace


class IfcFaceSurface(IfcFace):
    meta = {
        'namespace': 'ifc2x3'
    }

    FaceSurface = DynamicField()
    SameSense = DynamicField()
