from mongoengine import DynamicField
from .ifc_topological_representation_item import IfcTopologicalRepresentationItem


class IfcFaceBound(IfcTopologicalRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Bound = DynamicField()
    Orientation = DynamicField()
