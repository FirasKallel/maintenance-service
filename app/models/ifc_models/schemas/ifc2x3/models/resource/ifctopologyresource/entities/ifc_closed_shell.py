from mongoengine import DynamicField
from .ifc_connected_face_set import IfcConnectedFaceSet


class IfcClosedShell(IfcConnectedFaceSet):
    meta = {
        'namespace': 'ifc2x3'
    }

