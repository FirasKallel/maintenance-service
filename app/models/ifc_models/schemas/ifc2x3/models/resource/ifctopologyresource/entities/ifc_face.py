from mongoengine import DynamicField
from .ifc_topological_representation_item import IfcTopologicalRepresentationItem


class IfcFace(IfcTopologicalRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Bounds = DynamicField()
