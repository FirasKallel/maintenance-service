from mongoengine import DynamicField
from .ifc_edge import IfcEdge


class IfcOrientedEdge(IfcEdge):
    meta = {
        'namespace': 'ifc2x3'
    }

    EdgeElement = DynamicField()
    Orientation = DynamicField()
