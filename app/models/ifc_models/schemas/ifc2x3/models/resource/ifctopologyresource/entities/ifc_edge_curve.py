from mongoengine import DynamicField
from .ifc_edge import IfcEdge


class IfcEdgeCurve(IfcEdge):
    meta = {
        'namespace': 'ifc2x3'
    }

    EdgeGeometry = DynamicField()
    SameSense = DynamicField()
