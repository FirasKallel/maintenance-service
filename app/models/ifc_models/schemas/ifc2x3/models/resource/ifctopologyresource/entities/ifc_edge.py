from mongoengine import DynamicField
from .ifc_topological_representation_item import IfcTopologicalRepresentationItem


class IfcEdge(IfcTopologicalRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    EdgeStart = DynamicField()
    EdgeEnd = DynamicField()
