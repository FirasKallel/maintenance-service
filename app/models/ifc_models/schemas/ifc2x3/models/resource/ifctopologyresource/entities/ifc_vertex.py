from mongoengine import DynamicField
from .ifc_topological_representation_item import IfcTopologicalRepresentationItem


class IfcVertex(IfcTopologicalRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

