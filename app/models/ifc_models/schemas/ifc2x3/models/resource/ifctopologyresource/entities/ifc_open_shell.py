from mongoengine import DynamicField
from .ifc_connected_face_set import IfcConnectedFaceSet


class IfcOpenShell(IfcConnectedFaceSet):
    meta = {
        'namespace': 'ifc2x3'
    }

