from mongoengine import DynamicField
from .ifc_face_bound import IfcFaceBound


class IfcFaceOuterBound(IfcFaceBound):
    meta = {
        'namespace': 'ifc2x3'
    }

