from mongoengine import DynamicField
from .ifc_loop import IfcLoop


class IfcVertexLoop(IfcLoop):
    meta = {
        'namespace': 'ifc2x3'
    }

    LoopVertex = DynamicField()
