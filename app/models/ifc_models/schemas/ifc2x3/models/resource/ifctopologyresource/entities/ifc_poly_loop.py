from mongoengine import DynamicField
from .ifc_loop import IfcLoop


class IfcPolyLoop(IfcLoop):
    meta = {
        'namespace': 'ifc2x3'
    }

    Polygon = DynamicField()
