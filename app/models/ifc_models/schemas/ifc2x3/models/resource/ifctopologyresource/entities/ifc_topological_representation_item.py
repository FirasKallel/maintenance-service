from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_representation_item import IfcRepresentationItem


class IfcTopologicalRepresentationItem(IfcRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

