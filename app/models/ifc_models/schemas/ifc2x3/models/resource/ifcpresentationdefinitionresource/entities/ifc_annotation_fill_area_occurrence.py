from mongoengine import DynamicField
from .ifc_annotation_occurrence import IfcAnnotationOccurrence


class IfcAnnotationFillAreaOccurrence(IfcAnnotationOccurrence):
    meta = {
        'namespace': 'ifc2x3'
    }

    FillStyleTarget = DynamicField()
    GlobalOrLocal = DynamicField()
