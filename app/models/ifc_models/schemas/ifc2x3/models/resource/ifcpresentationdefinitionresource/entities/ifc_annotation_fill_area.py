from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcAnnotationFillArea(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    OuterBoundary = DynamicField()
    InnerBoundaries = DynamicField()
