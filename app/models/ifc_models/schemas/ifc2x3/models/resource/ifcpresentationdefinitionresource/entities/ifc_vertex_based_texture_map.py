from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcVertexBasedTextureMap(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    TextureVertices = DynamicField()
    TexturePoints = DynamicField()
