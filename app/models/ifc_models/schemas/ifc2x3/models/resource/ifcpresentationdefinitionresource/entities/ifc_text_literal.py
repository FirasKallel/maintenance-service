from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcTextLiteral(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Literal = DynamicField()
    Placement = DynamicField()
    Path = DynamicField()
