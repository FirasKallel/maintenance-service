from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcAnnotationSurface(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Item = DynamicField()
    TextureCoordinates = DynamicField()
