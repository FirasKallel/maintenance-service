from mongoengine import DynamicField
from .ifc_text_literal import IfcTextLiteral


class IfcTextLiteralWithExtent(IfcTextLiteral):
    meta = {
        'namespace': 'ifc2x3'
    }

    Extent = DynamicField()
    BoxAlignment = DynamicField()
