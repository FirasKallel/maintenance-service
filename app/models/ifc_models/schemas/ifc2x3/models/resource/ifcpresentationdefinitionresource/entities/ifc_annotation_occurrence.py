from mongoengine import DynamicField
from ...ifcpresentationappearanceresource.entities.ifc_styled_item import IfcStyledItem


class IfcAnnotationOccurrence(IfcStyledItem):
    meta = {
        'namespace': 'ifc2x3'
    }

