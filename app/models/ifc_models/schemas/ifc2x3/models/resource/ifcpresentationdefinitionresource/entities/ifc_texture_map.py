from mongoengine import DynamicField
from .ifc_texture_coordinate import IfcTextureCoordinate


class IfcTextureMap(IfcTextureCoordinate):
    meta = {
        'namespace': 'ifc2x3'
    }

    TextureMaps = DynamicField()
