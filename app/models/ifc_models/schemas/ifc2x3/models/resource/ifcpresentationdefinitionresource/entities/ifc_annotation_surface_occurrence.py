from mongoengine import DynamicField
from .ifc_annotation_occurrence import IfcAnnotationOccurrence


class IfcAnnotationSurfaceOccurrence(IfcAnnotationOccurrence):
    meta = {
        'namespace': 'ifc2x3'
    }

