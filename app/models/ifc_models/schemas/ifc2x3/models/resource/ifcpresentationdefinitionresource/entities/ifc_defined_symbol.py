from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcDefinedSymbol(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Definition = DynamicField()
    Target = DynamicField()
