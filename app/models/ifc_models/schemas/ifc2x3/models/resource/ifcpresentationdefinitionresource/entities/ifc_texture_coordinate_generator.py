from mongoengine import DynamicField
from .ifc_texture_coordinate import IfcTextureCoordinate


class IfcTextureCoordinateGenerator(IfcTextureCoordinate):
    meta = {
        'namespace': 'ifc2x3'
    }

    Mode = DynamicField()
    Parameter = DynamicField()
