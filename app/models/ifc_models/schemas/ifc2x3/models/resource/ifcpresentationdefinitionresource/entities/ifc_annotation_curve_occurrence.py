from mongoengine import DynamicField
from .ifc_annotation_occurrence import IfcAnnotationOccurrence


class IfcAnnotationCurveOccurrence(IfcAnnotationOccurrence):
    meta = {
        'namespace': 'ifc2x3'
    }

