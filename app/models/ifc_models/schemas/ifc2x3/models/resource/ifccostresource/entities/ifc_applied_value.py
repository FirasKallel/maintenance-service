from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcAppliedValue(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    AppliedValue = DynamicField()
    UnitBasis = DynamicField()
    ApplicableDate = DynamicField()
    FixedUntilDate = DynamicField()
