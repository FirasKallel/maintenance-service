from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcCurrencyRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingMonetaryUnit = DynamicField()
    RelatedMonetaryUnit = DynamicField()
    ExchangeRate = DynamicField()
    RateDateTime = DynamicField()
    RateSource = DynamicField()
