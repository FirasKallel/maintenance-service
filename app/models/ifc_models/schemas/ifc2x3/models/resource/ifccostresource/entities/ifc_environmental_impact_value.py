from mongoengine import DynamicField
from .ifc_applied_value import IfcAppliedValue


class IfcEnvironmentalImpactValue(IfcAppliedValue):
    meta = {
        'namespace': 'ifc2x3'
    }

    ImpactType = DynamicField()
    Category = DynamicField()
    UserDefinedCategory = DynamicField()
