from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcReferencesValueDocument(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ReferencedDocument = DynamicField()
    ReferencingValues = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
