from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcAppliedValueRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ComponentOfTotal = DynamicField()
    Components = DynamicField()
    ArithmeticOperator = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
