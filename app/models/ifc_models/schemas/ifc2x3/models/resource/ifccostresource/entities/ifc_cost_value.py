from mongoengine import DynamicField
from .ifc_applied_value import IfcAppliedValue


class IfcCostValue(IfcAppliedValue):
    meta = {
        'namespace': 'ifc2x3'
    }

    CostType = DynamicField()
    Condition = DynamicField()
