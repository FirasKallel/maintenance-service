from mongoengine import DynamicField
from .ifc_physical_simple_quantity import IfcPhysicalSimpleQuantity


class IfcQuantityTime(IfcPhysicalSimpleQuantity):
    meta = {
        'namespace': 'ifc2x3'
    }

    TimeValue = DynamicField()
