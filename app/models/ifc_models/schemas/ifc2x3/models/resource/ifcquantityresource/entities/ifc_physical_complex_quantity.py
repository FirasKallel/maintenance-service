from mongoengine import DynamicField
from .ifc_physical_quantity import IfcPhysicalQuantity


class IfcPhysicalComplexQuantity(IfcPhysicalQuantity):
    meta = {
        'namespace': 'ifc2x3'
    }

    HasQuantities = DynamicField()
    Discrimination = DynamicField()
    Quality = DynamicField()
    Usage = DynamicField()
