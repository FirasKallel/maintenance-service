from mongoengine import DynamicField
from .ifc_physical_quantity import IfcPhysicalQuantity


class IfcPhysicalSimpleQuantity(IfcPhysicalQuantity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Unit = DynamicField()
