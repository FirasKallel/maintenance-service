from mongoengine import DynamicField
from .ifc_physical_simple_quantity import IfcPhysicalSimpleQuantity


class IfcQuantityArea(IfcPhysicalSimpleQuantity):
    meta = {
        'namespace': 'ifc2x3'
    }

    AreaValue = DynamicField()
