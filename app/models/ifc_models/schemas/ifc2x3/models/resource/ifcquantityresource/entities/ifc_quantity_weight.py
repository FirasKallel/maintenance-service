from mongoengine import DynamicField
from .ifc_physical_simple_quantity import IfcPhysicalSimpleQuantity


class IfcQuantityWeight(IfcPhysicalSimpleQuantity):
    meta = {
        'namespace': 'ifc2x3'
    }

    WeightValue = DynamicField()
