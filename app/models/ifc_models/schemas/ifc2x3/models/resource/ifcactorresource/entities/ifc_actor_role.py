from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcActorRole(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Role = DynamicField()
    UserDefinedRole = DynamicField()
    Description = DynamicField()
