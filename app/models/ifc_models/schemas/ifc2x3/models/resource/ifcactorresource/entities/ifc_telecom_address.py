from mongoengine import DynamicField
from .ifc_address import IfcAddress


class IfcTelecomAddress(IfcAddress):
    meta = {
        'namespace': 'ifc2x3'
    }

    TelephoneNumbers = DynamicField()
    FacsimileNumbers = DynamicField()
    PagerNumber = DynamicField()
    ElectronicMailAddresses = DynamicField()
    WWWHomePageURL = DynamicField()
