from mongoengine import DynamicField
from .ifc_address import IfcAddress


class IfcPostalAddress(IfcAddress):
    meta = {
        'namespace': 'ifc2x3'
    }

    InternalLocation = DynamicField()
    AddressLines = DynamicField()
    PostalBox = DynamicField()
    Town = DynamicField()
    Region = DynamicField()
    PostalCode = DynamicField()
    Country = DynamicField()
