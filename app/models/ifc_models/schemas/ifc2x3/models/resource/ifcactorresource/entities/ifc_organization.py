from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcOrganization(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Id = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
    Roles = DynamicField()
    Addresses = DynamicField()
