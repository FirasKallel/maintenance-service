from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPersonAndOrganization(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ThePerson = DynamicField()
    TheOrganization = DynamicField()
    Roles = DynamicField()
