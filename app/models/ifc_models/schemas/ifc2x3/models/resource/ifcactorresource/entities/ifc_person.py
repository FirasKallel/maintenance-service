from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPerson(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Id = DynamicField()
    FamilyName = DynamicField()
    GivenName = DynamicField()
    MiddleNames = DynamicField()
    PrefixTitles = DynamicField()
    SuffixTitles = DynamicField()
    Roles = DynamicField()
    Addresses = DynamicField()
