from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcAddress(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Purpose = DynamicField()
    Description = DynamicField()
    UserDefinedPurpose = DynamicField()