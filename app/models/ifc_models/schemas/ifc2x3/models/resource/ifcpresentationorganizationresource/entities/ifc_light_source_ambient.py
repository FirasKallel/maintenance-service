from mongoengine import DynamicField
from .ifc_light_source import IfcLightSource


class IfcLightSourceAmbient(IfcLightSource):
    meta = {
        'namespace': 'ifc2x3'
    }

