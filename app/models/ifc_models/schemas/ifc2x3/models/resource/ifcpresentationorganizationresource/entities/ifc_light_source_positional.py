from mongoengine import DynamicField
from .ifc_light_source import IfcLightSource


class IfcLightSourcePositional(IfcLightSource):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
    Radius = DynamicField()
    ConstantAttenuation = DynamicField()
    DistanceAttenuation = DynamicField()
    QuadricAttenuation = DynamicField()
