from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcLightDistributionData(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    MainPlaneAngle = DynamicField()
    SecondaryPlaneAngle = DynamicField()
    LuminousIntensity = DynamicField()
