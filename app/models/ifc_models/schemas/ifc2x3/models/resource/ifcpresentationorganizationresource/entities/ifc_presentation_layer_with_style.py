from mongoengine import DynamicField
from .ifc_presentation_layer_assignment import IfcPresentationLayerAssignment


class IfcPresentationLayerWithStyle(IfcPresentationLayerAssignment):
    meta = {
        'namespace': 'ifc2x3'
    }

    LayerOn = DynamicField()
    LayerFrozen = DynamicField()
    LayerBlocked = DynamicField()
    LayerStyles = DynamicField()
