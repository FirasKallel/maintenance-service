from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcLightIntensityDistribution(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    LightDistributionCurve = DynamicField()
    DistributionData = DynamicField()
