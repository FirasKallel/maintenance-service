from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPresentationLayerAssignment(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    AssignedItems = DynamicField()
    Identifier = DynamicField()
