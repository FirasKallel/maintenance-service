from mongoengine import DynamicField
from .ifc_light_source_positional import IfcLightSourcePositional


class IfcLightSourceSpot(IfcLightSourcePositional):
    meta = {
        'namespace': 'ifc2x3'
    }

    Orientation = DynamicField()
    ConcentrationExponent = DynamicField()
    SpreadAngle = DynamicField()
    BeamWidthAngle = DynamicField()
