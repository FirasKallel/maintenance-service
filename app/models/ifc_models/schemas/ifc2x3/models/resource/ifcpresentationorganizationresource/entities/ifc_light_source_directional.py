from mongoengine import DynamicField
from .ifc_light_source import IfcLightSource


class IfcLightSourceDirectional(IfcLightSource):
    meta = {
        'namespace': 'ifc2x3'
    }

    Orientation = DynamicField()
