from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcLightSource(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    LightColour = DynamicField()
    AmbientIntensity = DynamicField()
    Intensity = DynamicField()
