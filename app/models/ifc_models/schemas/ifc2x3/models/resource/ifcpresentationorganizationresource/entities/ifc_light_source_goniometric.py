from mongoengine import DynamicField
from .ifc_light_source import IfcLightSource


class IfcLightSourceGoniometric(IfcLightSource):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
    ColourAppearance = DynamicField()
    ColourTemperature = DynamicField()
    LuminousFlux = DynamicField()
    LightEmissionSource = DynamicField()
    LightDistributionDataSource = DynamicField()
