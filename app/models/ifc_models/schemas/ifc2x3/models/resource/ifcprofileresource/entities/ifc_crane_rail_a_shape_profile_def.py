from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcCraneRailAShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    OverallHeight = DynamicField()
    BaseWidth2 = DynamicField()
    Radius = DynamicField()
    HeadWidth = DynamicField()
    HeadDepth2 = DynamicField()
    HeadDepth3 = DynamicField()
    WebThickness = DynamicField()
    BaseWidth4 = DynamicField()
    BaseDepth1 = DynamicField()
    BaseDepth2 = DynamicField()
    BaseDepth3 = DynamicField()
    CentreOfGravityInY = DynamicField()
