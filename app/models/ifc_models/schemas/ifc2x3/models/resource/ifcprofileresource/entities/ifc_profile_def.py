from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcProfileDef(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ProfileType = DynamicField()
    ProfileName = DynamicField()
