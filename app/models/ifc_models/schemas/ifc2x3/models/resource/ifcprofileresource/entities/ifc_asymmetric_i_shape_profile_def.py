from mongoengine import DynamicField
from .ifc_i_shape_profile_def import IfcIShapeProfileDef


class IfcAsymmetricIShapeProfileDef(IfcIShapeProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    TopFlangeWidth = DynamicField()
    TopFlangeThickness = DynamicField()
    TopFlangeFilletRadius = DynamicField()
    CentreOfGravityInY = DynamicField()
