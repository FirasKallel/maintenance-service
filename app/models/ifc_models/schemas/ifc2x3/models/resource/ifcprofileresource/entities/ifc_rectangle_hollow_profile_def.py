from mongoengine import DynamicField
from .ifc_rectangle_profile_def import IfcRectangleProfileDef


class IfcRectangleHollowProfileDef(IfcRectangleProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    WallThickness = DynamicField()
    InnerFilletRadius = DynamicField()
    OuterFilletRadius = DynamicField()
