from mongoengine import DynamicField
from .ifc_profile_def import IfcProfileDef


class IfcCompositeProfileDef(IfcProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Profiles = DynamicField()
    Label = DynamicField()
