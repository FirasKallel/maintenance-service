from mongoengine import DynamicField
from .ifc_profile_def import IfcProfileDef


class IfcArbitraryOpenProfileDef(IfcProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Curve = DynamicField()
