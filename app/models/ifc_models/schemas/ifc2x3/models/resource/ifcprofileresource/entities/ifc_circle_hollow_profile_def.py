from mongoengine import DynamicField
from .ifc_circle_profile_def import IfcCircleProfileDef


class IfcCircleHollowProfileDef(IfcCircleProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    WallThickness = DynamicField()
