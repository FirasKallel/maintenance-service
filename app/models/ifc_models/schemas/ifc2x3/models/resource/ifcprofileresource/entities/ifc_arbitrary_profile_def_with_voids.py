from mongoengine import DynamicField
from .ifc_arbitrary_closed_profile_def import IfcArbitraryClosedProfileDef


class IfcArbitraryProfileDefWithVoids(IfcArbitraryClosedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    InnerCurves = DynamicField()
