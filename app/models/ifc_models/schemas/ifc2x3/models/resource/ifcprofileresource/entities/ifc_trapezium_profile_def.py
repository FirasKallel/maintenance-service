from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcTrapeziumProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    BottomXDim = DynamicField()
    TopXDim = DynamicField()
    YDim = DynamicField()
    TopXOffset = DynamicField()
