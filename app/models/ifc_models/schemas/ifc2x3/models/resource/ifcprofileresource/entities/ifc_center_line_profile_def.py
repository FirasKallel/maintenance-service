from mongoengine import DynamicField
from .ifc_arbitrary_open_profile_def import IfcArbitraryOpenProfileDef


class IfcCenterLineProfileDef(IfcArbitraryOpenProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Thickness = DynamicField()
