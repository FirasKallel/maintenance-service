from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcLShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Depth = DynamicField()
    Width = DynamicField()
    Thickness = DynamicField()
    FilletRadius = DynamicField()
    EdgeRadius = DynamicField()
    LegSlope = DynamicField()
    CentreOfGravityInX = DynamicField()
    CentreOfGravityInY = DynamicField()
