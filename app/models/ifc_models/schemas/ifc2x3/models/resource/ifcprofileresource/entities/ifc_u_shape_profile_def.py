from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcUShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Depth = DynamicField()
    FlangeWidth = DynamicField()
    WebThickness = DynamicField()
    FlangeThickness = DynamicField()
    FilletRadius = DynamicField()
    EdgeRadius = DynamicField()
    FlangeSlope = DynamicField()
    CentreOfGravityInX = DynamicField()
