from mongoengine import DynamicField
from .ifc_profile_def import IfcProfileDef


class IfcDerivedProfileDef(IfcProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    ParentProfile = DynamicField()
    Operator = DynamicField()
    Label = DynamicField()
