from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcCraneRailFShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    OverallHeight = DynamicField()
    HeadWidth = DynamicField()
    Radius = DynamicField()
    HeadDepth2 = DynamicField()
    HeadDepth3 = DynamicField()
    WebThickness = DynamicField()
    BaseDepth1 = DynamicField()
    BaseDepth2 = DynamicField()
    CentreOfGravityInY = DynamicField()
