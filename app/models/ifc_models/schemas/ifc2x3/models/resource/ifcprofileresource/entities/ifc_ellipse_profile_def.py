from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcEllipseProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    SemiAxis1 = DynamicField()
    SemiAxis2 = DynamicField()
