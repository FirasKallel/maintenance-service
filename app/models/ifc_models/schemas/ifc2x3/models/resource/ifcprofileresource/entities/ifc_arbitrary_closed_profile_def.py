from mongoengine import DynamicField
from .ifc_profile_def import IfcProfileDef


class IfcArbitraryClosedProfileDef(IfcProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    OuterCurve = DynamicField()
