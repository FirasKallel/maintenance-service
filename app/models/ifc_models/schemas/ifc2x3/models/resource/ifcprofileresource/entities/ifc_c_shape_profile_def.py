from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcCShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Depth = DynamicField()
    Width = DynamicField()
    WallThickness = DynamicField()
    Girth = DynamicField()
    InternalFilletRadius = DynamicField()
    CentreOfGravityInX = DynamicField()
