from mongoengine import DynamicField
from .ifc_profile_def import IfcProfileDef


class IfcParameterizedProfileDef(IfcProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
