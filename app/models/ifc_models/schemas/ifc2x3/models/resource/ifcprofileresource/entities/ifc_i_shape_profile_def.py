from mongoengine import DynamicField
from .ifc_parameterized_profile_def import IfcParameterizedProfileDef


class IfcIShapeProfileDef(IfcParameterizedProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    OverallWidth = DynamicField()
    OverallDepth = DynamicField()
    WebThickness = DynamicField()
    FlangeThickness = DynamicField()
    FilletRadius = DynamicField()
