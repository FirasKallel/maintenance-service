from mongoengine import DynamicField
from .ifc_rectangle_profile_def import IfcRectangleProfileDef


class IfcRoundedRectangleProfileDef(IfcRectangleProfileDef):
    meta = {
        'namespace': 'ifc2x3'
    }

    RoundingRadius = DynamicField()
