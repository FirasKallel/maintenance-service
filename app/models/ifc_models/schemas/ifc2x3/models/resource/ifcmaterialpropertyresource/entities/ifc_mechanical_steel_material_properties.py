from mongoengine import DynamicField
from .ifc_mechanical_material_properties import IfcMechanicalMaterialProperties


class IfcMechanicalSteelMaterialProperties(IfcMechanicalMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    YieldStress = DynamicField()
    UltimateStress = DynamicField()
    UltimateStrain = DynamicField()
    HardeningModule = DynamicField()
    ProportionalStress = DynamicField()
    PlasticStrain = DynamicField()
    Relaxations = DynamicField()
