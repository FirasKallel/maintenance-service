from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcProductsOfCombustionProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    SpecificHeatCapacity = DynamicField()
    N20Content = DynamicField()
    COContent = DynamicField()
    CO2Content = DynamicField()
