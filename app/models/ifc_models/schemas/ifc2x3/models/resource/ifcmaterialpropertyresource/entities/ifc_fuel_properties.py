from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcFuelProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    CombustionTemperature = DynamicField()
    CarbonContent = DynamicField()
    LowerHeatingValue = DynamicField()
    HigherHeatingValue = DynamicField()
