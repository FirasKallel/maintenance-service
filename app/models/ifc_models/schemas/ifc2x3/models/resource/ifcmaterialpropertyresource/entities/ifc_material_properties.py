from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialProperties(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Material = DynamicField()
