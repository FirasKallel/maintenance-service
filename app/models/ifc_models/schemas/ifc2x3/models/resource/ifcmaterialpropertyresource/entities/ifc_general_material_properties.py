from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcGeneralMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    MolecularWeight = DynamicField()
    Porosity = DynamicField()
    MassDensity = DynamicField()
