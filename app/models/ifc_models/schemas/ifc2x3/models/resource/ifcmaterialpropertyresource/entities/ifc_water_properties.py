from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcWaterProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    IsPotable = DynamicField()
    Hardness = DynamicField()
    AlkalinityConcentration = DynamicField()
    AcidityConcentration = DynamicField()
    ImpuritiesContent = DynamicField()
    PHLevel = DynamicField()
    DissolvedSolidsContent = DynamicField()
