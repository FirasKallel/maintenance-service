from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcHygroscopicMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    UpperVaporResistanceFactor = DynamicField()
    LowerVaporResistanceFactor = DynamicField()
    IsothermalMoistureCapacity = DynamicField()
    VaporPermeability = DynamicField()
    MoistureDiffusivity = DynamicField()
