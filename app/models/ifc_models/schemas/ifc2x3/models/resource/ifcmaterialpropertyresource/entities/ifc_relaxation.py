from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcRelaxation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelaxationValue = DynamicField()
    InitialStress = DynamicField()
