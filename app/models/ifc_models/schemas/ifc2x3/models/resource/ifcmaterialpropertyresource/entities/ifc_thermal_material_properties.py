from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcThermalMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    SpecificHeatCapacity = DynamicField()
    BoilingPoint = DynamicField()
    FreezingPoint = DynamicField()
    ThermalConductivity = DynamicField()
