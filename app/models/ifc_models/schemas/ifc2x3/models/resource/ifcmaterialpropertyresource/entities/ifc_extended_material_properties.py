from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcExtendedMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    ExtendedProperties = DynamicField()
    Description = DynamicField()
    Name = DynamicField()
