from mongoengine import DynamicField
from .ifc_mechanical_material_properties import IfcMechanicalMaterialProperties


class IfcMechanicalConcreteMaterialProperties(IfcMechanicalMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    CompressiveStrength = DynamicField()
    MaxAggregateSize = DynamicField()
    AdmixturesDescription = DynamicField()
    Workability = DynamicField()
    ProtectivePoreRatio = DynamicField()
    WaterImpermeability = DynamicField()
