from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcMechanicalMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    DynamicViscosity = DynamicField()
    YoungModulus = DynamicField()
    ShearModulus = DynamicField()
    PoissonRatio = DynamicField()
    ThermalExpansionCoefficient = DynamicField()
