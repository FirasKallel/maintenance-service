from mongoengine import DynamicField
from .ifc_material_properties import IfcMaterialProperties


class IfcOpticalMaterialProperties(IfcMaterialProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    VisibleTransmittance = DynamicField()
    SolarTransmittance = DynamicField()
    ThermalIrTransmittance = DynamicField()
    ThermalIrEmissivityBack = DynamicField()
    ThermalIrEmissivityFront = DynamicField()
    VisibleReflectanceBack = DynamicField()
    VisibleReflectanceFront = DynamicField()
    SolarReflectanceFront = DynamicField()
    SolarReflectanceBack = DynamicField()
