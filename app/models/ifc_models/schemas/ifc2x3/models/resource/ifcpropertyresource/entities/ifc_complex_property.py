from mongoengine import DynamicField
from .ifc_property import IfcProperty


class IfcComplexProperty(IfcProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    UsageName = DynamicField()
    HasProperties = DynamicField()
