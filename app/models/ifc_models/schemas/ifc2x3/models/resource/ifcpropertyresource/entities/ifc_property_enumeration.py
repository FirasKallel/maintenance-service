from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPropertyEnumeration(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    EnumerationValues = DynamicField()
    Unit = DynamicField()
