from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertyBoundedValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    UpperBoundValue = DynamicField()
    LowerBoundValue = DynamicField()
    Unit = DynamicField()
