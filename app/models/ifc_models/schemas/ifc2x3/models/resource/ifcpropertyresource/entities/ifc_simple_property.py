from mongoengine import DynamicField
from .ifc_property import IfcProperty


class IfcSimpleProperty(IfcProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

