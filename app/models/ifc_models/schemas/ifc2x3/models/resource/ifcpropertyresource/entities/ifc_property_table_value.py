from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertyTableValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    DefiningValues = DynamicField()
    DefinedValues = DynamicField()
    Expression = DynamicField()
    DefiningUnit = DynamicField()
    DefinedUnit = DynamicField()
