from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertyListValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    ListValues = DynamicField()
    Unit = DynamicField()
