from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertyReferenceValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    UsageName = DynamicField()
    PropertyReference = DynamicField()
