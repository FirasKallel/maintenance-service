from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertyEnumeratedValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    EnumerationValues = DynamicField()
    EnumerationReference = DynamicField()
