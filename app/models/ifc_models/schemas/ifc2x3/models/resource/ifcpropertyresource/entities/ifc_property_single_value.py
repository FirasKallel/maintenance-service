from mongoengine import DynamicField
from .ifc_simple_property import IfcSimpleProperty


class IfcPropertySingleValue(IfcSimpleProperty):
    meta = {
        'namespace': 'ifc2x3'
    }

    NominalValue = DynamicField()
    Unit = DynamicField()
