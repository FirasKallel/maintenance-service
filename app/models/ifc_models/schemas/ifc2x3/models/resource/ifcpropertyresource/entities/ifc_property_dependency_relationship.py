from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPropertyDependencyRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DependingProperty = DynamicField()
    DependantProperty = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
    Expression = DynamicField()
