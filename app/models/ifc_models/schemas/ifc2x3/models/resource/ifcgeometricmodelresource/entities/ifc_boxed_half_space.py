from mongoengine import DynamicField
from .ifc_half_space_solid import IfcHalfSpaceSolid


class IfcBoxedHalfSpace(IfcHalfSpaceSolid):
    meta = {
        'namespace': 'ifc2x3'
    }

    Enclosure = DynamicField()
