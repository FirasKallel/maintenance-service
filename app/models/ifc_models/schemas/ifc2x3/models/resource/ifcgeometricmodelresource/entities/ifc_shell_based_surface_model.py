from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcShellBasedSurfaceModel(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    SbsmBoundary = DynamicField()
