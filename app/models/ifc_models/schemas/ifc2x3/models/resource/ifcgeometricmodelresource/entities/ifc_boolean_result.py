from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcBooleanResult(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Operator = DynamicField()
    FirstOperand = DynamicField()
    SecondOperand = DynamicField()
