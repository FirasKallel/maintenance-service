from mongoengine import DynamicField
from .ifc_swept_area_solid import IfcSweptAreaSolid


class IfcSurfaceCurveSweptAreaSolid(IfcSweptAreaSolid):
    meta = {
        'namespace': 'ifc2x3'
    }

    Directrix = DynamicField()
    StartParam = DynamicField()
    EndParam = DynamicField()
    ReferenceSurface = DynamicField()
