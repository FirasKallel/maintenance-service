from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcCsgPrimitive3D(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
