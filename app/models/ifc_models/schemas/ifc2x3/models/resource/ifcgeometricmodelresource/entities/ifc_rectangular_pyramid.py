from mongoengine import DynamicField
from .ifc_csg_primitive3_d import IfcCsgPrimitive3D


class IfcRectangularPyramid(IfcCsgPrimitive3D):
    meta = {
        'namespace': 'ifc2x3'
    }

    XLength = DynamicField()
    YLength = DynamicField()
    Height = DynamicField()
