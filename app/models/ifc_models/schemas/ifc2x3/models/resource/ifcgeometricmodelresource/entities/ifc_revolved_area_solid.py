from mongoengine import DynamicField
from .ifc_swept_area_solid import IfcSweptAreaSolid


class IfcRevolvedAreaSolid(IfcSweptAreaSolid):
    meta = {
        'namespace': 'ifc2x3'
    }

    Axis = DynamicField()
    Angle = DynamicField()
