from mongoengine import DynamicField
from .ifc_solid_model import IfcSolidModel


class IfcCsgSolid(IfcSolidModel):
    meta = {
        'namespace': 'ifc2x3'
    }

    TreeRootExpression = DynamicField()
