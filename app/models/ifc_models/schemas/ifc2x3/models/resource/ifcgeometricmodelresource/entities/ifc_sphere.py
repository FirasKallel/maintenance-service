from mongoengine import DynamicField
from .ifc_csg_primitive3_d import IfcCsgPrimitive3D


class IfcSphere(IfcCsgPrimitive3D):
    meta = {
        'namespace': 'ifc2x3'
    }

    Radius = DynamicField()
