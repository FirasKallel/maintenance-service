from mongoengine import DynamicField
from .ifc_solid_model import IfcSolidModel


class IfcSweptAreaSolid(IfcSolidModel):
    meta = {
        'namespace': 'ifc2x3'
    }

    SweptArea = DynamicField()
    Position = DynamicField()
