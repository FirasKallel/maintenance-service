from mongoengine import DynamicField
from .ifc_solid_model import IfcSolidModel


class IfcManifoldSolidBrep(IfcSolidModel):
    meta = {
        'namespace': 'ifc2x3'
    }

    Outer = DynamicField()
