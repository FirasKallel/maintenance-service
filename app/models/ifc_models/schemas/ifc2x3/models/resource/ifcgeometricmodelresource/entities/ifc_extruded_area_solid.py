from mongoengine import DynamicField
from .ifc_swept_area_solid import IfcSweptAreaSolid


class IfcExtrudedAreaSolid(IfcSweptAreaSolid):
    meta = {
        'namespace': 'ifc2x3'
    }

    ExtrudedDirection = DynamicField()
    Depth = DynamicField()
