from mongoengine import DynamicField
from .ifc_manifold_solid_brep import IfcManifoldSolidBrep


class IfcFacetedBrep(IfcManifoldSolidBrep):
    meta = {
        'namespace': 'ifc2x3'
    }

