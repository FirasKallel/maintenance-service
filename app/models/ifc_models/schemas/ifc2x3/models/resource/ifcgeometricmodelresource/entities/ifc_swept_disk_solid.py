from mongoengine import DynamicField
from .ifc_solid_model import IfcSolidModel


class IfcSweptDiskSolid(IfcSolidModel):
    meta = {
        'namespace': 'ifc2x3'
    }

    Directrix = DynamicField()
    Radius = DynamicField()
    InnerRadius = DynamicField()
    StartParam = DynamicField()
    EndParam = DynamicField()
