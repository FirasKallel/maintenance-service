from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcSectionedSpine(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    SpineCurve = DynamicField()
    CrossSections = DynamicField()
    CrossSectionPositions = DynamicField()
