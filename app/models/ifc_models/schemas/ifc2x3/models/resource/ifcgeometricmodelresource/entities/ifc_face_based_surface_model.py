from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcFaceBasedSurfaceModel(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    FbsmFaces = DynamicField()
