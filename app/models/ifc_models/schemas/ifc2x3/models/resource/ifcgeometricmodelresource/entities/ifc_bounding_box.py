from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcBoundingBox(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Corner = DynamicField()
    XDim = DynamicField()
    YDim = DynamicField()
    ZDim = DynamicField()
