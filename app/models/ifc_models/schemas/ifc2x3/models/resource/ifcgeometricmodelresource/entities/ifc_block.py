from mongoengine import DynamicField
from .ifc_csg_primitive3_d import IfcCsgPrimitive3D


class IfcBlock(IfcCsgPrimitive3D):
    meta = {
        'namespace': 'ifc2x3'
    }

    XLength = DynamicField()
    YLength = DynamicField()
    ZLength = DynamicField()
