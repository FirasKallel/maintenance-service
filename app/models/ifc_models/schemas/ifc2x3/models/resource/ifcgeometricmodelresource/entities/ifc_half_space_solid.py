from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcHalfSpaceSolid(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    BaseSurface = DynamicField()
    AgreementFlag = DynamicField()
