from mongoengine import DynamicField
from .ifc_boolean_result import IfcBooleanResult


class IfcBooleanClippingResult(IfcBooleanResult):
    meta = {
        'namespace': 'ifc2x3'
    }

