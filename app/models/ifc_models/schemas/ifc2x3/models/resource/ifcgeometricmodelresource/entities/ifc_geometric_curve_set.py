from mongoengine import DynamicField
from .ifc_geometric_set import IfcGeometricSet


class IfcGeometricCurveSet(IfcGeometricSet):
    meta = {
        'namespace': 'ifc2x3'
    }

