from mongoengine import DynamicField
from .ifc_csg_primitive3_d import IfcCsgPrimitive3D


class IfcRightCircularCone(IfcCsgPrimitive3D):
    meta = {
        'namespace': 'ifc2x3'
    }

    Height = DynamicField()
    BottomRadius = DynamicField()
