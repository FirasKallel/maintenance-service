from mongoengine import DynamicField
from .ifc_half_space_solid import IfcHalfSpaceSolid


class IfcPolygonalBoundedHalfSpace(IfcHalfSpaceSolid):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
    PolygonalBoundary = DynamicField()
