from mongoengine import DynamicField
from .ifc_manifold_solid_brep import IfcManifoldSolidBrep


class IfcFacetedBrepWithVoids(IfcManifoldSolidBrep):
    meta = {
        'namespace': 'ifc2x3'
    }

    Voids = DynamicField()
