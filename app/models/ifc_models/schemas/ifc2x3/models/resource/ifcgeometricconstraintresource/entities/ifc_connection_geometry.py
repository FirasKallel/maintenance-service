from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcConnectionGeometry(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

