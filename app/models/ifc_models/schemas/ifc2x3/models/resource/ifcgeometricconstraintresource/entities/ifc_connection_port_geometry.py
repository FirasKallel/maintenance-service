from mongoengine import DynamicField
from .ifc_connection_geometry import IfcConnectionGeometry


class IfcConnectionPortGeometry(IfcConnectionGeometry):
    meta = {
        'namespace': 'ifc2x3'
    }

    LocationAtRelatingElement = DynamicField()
    LocationAtRelatedElement = DynamicField()
    ProfileOfPort = DynamicField()
