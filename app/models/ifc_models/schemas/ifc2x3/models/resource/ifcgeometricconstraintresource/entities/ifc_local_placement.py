from mongoengine import DynamicField
from .ifc_object_placement import IfcObjectPlacement


class IfcLocalPlacement(IfcObjectPlacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    PlacementRelTo = DynamicField()
    RelativePlacement = DynamicField()
