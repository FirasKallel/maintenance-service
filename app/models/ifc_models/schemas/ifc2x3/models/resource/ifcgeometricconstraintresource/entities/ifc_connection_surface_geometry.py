from mongoengine import DynamicField
from .ifc_connection_geometry import IfcConnectionGeometry


class IfcConnectionSurfaceGeometry(IfcConnectionGeometry):
    meta = {
        'namespace': 'ifc2x3'
    }

    SurfaceOnRelatingElement = DynamicField()
    SurfaceOnRelatedElement = DynamicField()
