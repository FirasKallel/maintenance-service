from mongoengine import DynamicField
from .ifc_connection_geometry import IfcConnectionGeometry


class IfcConnectionPointGeometry(IfcConnectionGeometry):
    meta = {
        'namespace': 'ifc2x3'
    }

    PointOnRelatingElement = DynamicField()
    PointOnRelatedElement = DynamicField()
