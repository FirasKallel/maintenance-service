from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcGridAxis(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    AxisTag = DynamicField()
    AxisCurve = DynamicField()
    SameSense = DynamicField()
