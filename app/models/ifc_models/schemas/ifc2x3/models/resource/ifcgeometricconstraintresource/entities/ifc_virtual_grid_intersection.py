from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcVirtualGridIntersection(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    IntersectingAxes = DynamicField()
    OffsetDistances = DynamicField()
