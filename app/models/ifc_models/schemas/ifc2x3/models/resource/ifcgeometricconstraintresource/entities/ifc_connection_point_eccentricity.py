from mongoengine import DynamicField
from .ifc_connection_point_geometry import IfcConnectionPointGeometry


class IfcConnectionPointEccentricity(IfcConnectionPointGeometry):
    meta = {
        'namespace': 'ifc2x3'
    }

    EccentricityInX = DynamicField()
    EccentricityInY = DynamicField()
    EccentricityInZ = DynamicField()
