from mongoengine import DynamicField
from .ifc_object_placement import IfcObjectPlacement


class IfcGridPlacement(IfcObjectPlacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    PlacementLocation = DynamicField()
    PlacementRefDirection = DynamicField()
