from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSectionProperties(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    SectionType = DynamicField()
    StartProfile = DynamicField()
    EndProfile = DynamicField()
