from mongoengine import DynamicField
from .ifc_general_profile_properties import IfcGeneralProfileProperties


class IfcStructuralProfileProperties(IfcGeneralProfileProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    TorsionalConstantX = DynamicField()
    MomentOfInertiaYZ = DynamicField()
    MomentOfInertiaY = DynamicField()
    MomentOfInertiaZ = DynamicField()
    WarpingConstant = DynamicField()
    ShearCentreZ = DynamicField()
    ShearCentreY = DynamicField()
    ShearDeformationAreaZ = DynamicField()
    ShearDeformationAreaY = DynamicField()
    MaximumSectionModulusY = DynamicField()
    MinimumSectionModulusY = DynamicField()
    MaximumSectionModulusZ = DynamicField()
    MinimumSectionModulusZ = DynamicField()
    TorsionalSectionModulus = DynamicField()
    CentreOfGravityInX = DynamicField()
    CentreOfGravityInY = DynamicField()
