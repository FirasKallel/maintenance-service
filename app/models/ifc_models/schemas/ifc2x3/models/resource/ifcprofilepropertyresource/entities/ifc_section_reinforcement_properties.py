from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSectionReinforcementProperties(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    LongitudinalStartPosition = DynamicField()
    LongitudinalEndPosition = DynamicField()
    TransversePosition = DynamicField()
    ReinforcementRole = DynamicField()
    SectionDefinition = DynamicField()
    CrossSectionReinforcementDefinitions = DynamicField()
