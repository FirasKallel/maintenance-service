from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcReinforcementBarProperties(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    TotalCrossSectionArea = DynamicField()
    SteelGrade = DynamicField()
    BarSurface = DynamicField()
    EffectiveDepth = DynamicField()
    NominalBarDiameter = DynamicField()
    BarCount = DynamicField()
