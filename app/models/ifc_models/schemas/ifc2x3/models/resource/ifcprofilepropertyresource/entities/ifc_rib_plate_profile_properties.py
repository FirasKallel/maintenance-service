from mongoengine import DynamicField
from .ifc_profile_properties import IfcProfileProperties


class IfcRibPlateProfileProperties(IfcProfileProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    Thickness = DynamicField()
    RibHeight = DynamicField()
    RibWidth = DynamicField()
    RibSpacing = DynamicField()
    Direction = DynamicField()
