from mongoengine import DynamicField
from .ifc_profile_properties import IfcProfileProperties


class IfcGeneralProfileProperties(IfcProfileProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    PhysicalWeight = DynamicField()
    Perimeter = DynamicField()
    MinimumPlateThickness = DynamicField()
    MaximumPlateThickness = DynamicField()
    CrossSectionArea = DynamicField()
