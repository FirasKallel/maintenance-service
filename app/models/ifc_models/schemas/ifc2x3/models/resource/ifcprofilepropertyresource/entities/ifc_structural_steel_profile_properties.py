from mongoengine import DynamicField
from .ifc_structural_profile_properties import IfcStructuralProfileProperties


class IfcStructuralSteelProfileProperties(IfcStructuralProfileProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    ShearAreaZ = DynamicField()
    ShearAreaY = DynamicField()
    PlasticShapeFactorY = DynamicField()
    PlasticShapeFactorZ = DynamicField()
