from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcProfileProperties(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ProfileName = DynamicField()
    ProfileDefinition = DynamicField()
