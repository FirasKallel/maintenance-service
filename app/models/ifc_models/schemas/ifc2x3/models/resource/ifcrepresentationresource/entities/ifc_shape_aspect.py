from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcShapeAspect(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ShapeRepresentations = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
    ProductDefinitional = DynamicField()
    PartOfProductDefinitionShape = DynamicField()
