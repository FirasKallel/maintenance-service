from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcRepresentationContext(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ContextIdentifier = DynamicField()
    ContextType = DynamicField()
