from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcProductRepresentation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    Representations = DynamicField()
