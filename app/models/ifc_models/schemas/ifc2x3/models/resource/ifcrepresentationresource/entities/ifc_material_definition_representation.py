from mongoengine import DynamicField
from .ifc_product_representation import IfcProductRepresentation


class IfcMaterialDefinitionRepresentation(IfcProductRepresentation):
    meta = {
        'namespace': 'ifc2x3'
    }

    RepresentedMaterial = DynamicField()
