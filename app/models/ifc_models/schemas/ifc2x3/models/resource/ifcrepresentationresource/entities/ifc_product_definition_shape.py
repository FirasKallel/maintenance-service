from mongoengine import DynamicField
from .ifc_product_representation import IfcProductRepresentation


class IfcProductDefinitionShape(IfcProductRepresentation):
    meta = {
        'namespace': 'ifc2x3'
    }

