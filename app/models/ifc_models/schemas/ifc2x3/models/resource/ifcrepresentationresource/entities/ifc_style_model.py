from mongoengine import DynamicField
from .ifc_representation import IfcRepresentation


class IfcStyleModel(IfcRepresentation):
    meta = {
        'namespace': 'ifc2x3'
    }

