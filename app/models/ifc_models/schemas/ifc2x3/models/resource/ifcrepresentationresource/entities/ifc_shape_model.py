from mongoengine import DynamicField
from .ifc_representation import IfcRepresentation


class IfcShapeModel(IfcRepresentation):
    meta = {
        'namespace': 'ifc2x3'
    }

