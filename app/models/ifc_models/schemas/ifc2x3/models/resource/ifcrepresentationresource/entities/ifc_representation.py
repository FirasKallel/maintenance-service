from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcRepresentation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ContextOfItems = DynamicField()
    RepresentationIdentifier = DynamicField()
    RepresentationType = DynamicField()
    Items = DynamicField()
