from mongoengine import DynamicField
from .ifc_geometric_representation_context import IfcGeometricRepresentationContext


class IfcGeometricRepresentationSubContext(IfcGeometricRepresentationContext):
    meta = {
        'namespace': 'ifc2x3'
    }

    ParentContext = DynamicField()
    TargetScale = DynamicField()
    TargetView = DynamicField()
    UserDefinedTargetView = DynamicField()
