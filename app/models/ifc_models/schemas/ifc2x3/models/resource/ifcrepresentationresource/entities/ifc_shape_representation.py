from mongoengine import DynamicField
from .ifc_shape_model import IfcShapeModel


class IfcShapeRepresentation(IfcShapeModel):
    meta = {
        'namespace': 'ifc2x3'
    }

