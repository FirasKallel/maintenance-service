from mongoengine import DynamicField
from .ifc_style_model import IfcStyleModel


class IfcStyledRepresentation(IfcStyleModel):
    meta = {
        'namespace': 'ifc2x3'
    }

