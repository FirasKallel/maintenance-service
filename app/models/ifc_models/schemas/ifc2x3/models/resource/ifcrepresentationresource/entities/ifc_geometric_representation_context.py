from mongoengine import DynamicField
from .ifc_representation_context import IfcRepresentationContext


class IfcGeometricRepresentationContext(IfcRepresentationContext):
    meta = {
        'namespace': 'ifc2x3'
    }

    CoordinateSpaceDimension = DynamicField()
    Precision = DynamicField()
    WorldCoordinateSystem = DynamicField()
    TrueNorth = DynamicField()
