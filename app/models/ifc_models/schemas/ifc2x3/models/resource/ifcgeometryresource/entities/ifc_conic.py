from mongoengine import DynamicField
from .ifc_curve import IfcCurve


class IfcConic(IfcCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
