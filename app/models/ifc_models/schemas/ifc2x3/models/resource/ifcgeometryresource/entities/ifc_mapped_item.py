from mongoengine import DynamicField
from .ifc_representation_item import IfcRepresentationItem


class IfcMappedItem(IfcRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    MappingSource = DynamicField()
    MappingTarget = DynamicField()
