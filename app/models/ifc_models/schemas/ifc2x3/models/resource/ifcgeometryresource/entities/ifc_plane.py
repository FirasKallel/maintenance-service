from mongoengine import DynamicField
from .ifc_elementary_surface import IfcElementarySurface


class IfcPlane(IfcElementarySurface):
    meta = {
        'namespace': 'ifc2x3'
    }

