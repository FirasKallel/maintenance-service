from mongoengine import DynamicField
from .ifc_bezier_curve import IfcBezierCurve


class IfcRationalBezierCurve(IfcBezierCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    WeightsData = DynamicField()
