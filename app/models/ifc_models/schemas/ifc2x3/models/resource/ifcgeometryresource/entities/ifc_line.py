from mongoengine import DynamicField
from .ifc_curve import IfcCurve


class IfcLine(IfcCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    Pnt = DynamicField()
    Dir = DynamicField()
