from mongoengine import DynamicField
from .ifc_b_spline_curve import IfcBSplineCurve


class IfcBezierCurve(IfcBSplineCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

