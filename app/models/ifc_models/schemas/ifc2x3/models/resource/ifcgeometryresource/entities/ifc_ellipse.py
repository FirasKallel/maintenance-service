from mongoengine import DynamicField
from .ifc_conic import IfcConic


class IfcEllipse(IfcConic):
    meta = {
        'namespace': 'ifc2x3'
    }

    SemiAxis1 = DynamicField()
    SemiAxis2 = DynamicField()
