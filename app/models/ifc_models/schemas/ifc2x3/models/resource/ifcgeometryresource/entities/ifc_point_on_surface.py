from mongoengine import DynamicField
from .ifc_point import IfcPoint


class IfcPointOnSurface(IfcPoint):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisSurface = DynamicField()
    PointParameterU = DynamicField()
    PointParameterV = DynamicField()
