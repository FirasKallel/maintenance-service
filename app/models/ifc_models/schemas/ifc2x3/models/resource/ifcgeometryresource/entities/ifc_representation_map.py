from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcRepresentationMap(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    MappingOrigin = DynamicField()
    MappedRepresentation = DynamicField()
