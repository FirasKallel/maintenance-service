from mongoengine import DynamicField
from .ifc_surface import IfcSurface


class IfcElementarySurface(IfcSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    Position = DynamicField()
