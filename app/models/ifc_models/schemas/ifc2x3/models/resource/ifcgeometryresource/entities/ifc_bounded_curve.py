from mongoengine import DynamicField
from .ifc_curve import IfcCurve


class IfcBoundedCurve(IfcCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

