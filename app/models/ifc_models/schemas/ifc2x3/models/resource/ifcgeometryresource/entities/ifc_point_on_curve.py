from mongoengine import DynamicField
from .ifc_point import IfcPoint


class IfcPointOnCurve(IfcPoint):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisCurve = DynamicField()
    PointParameter = DynamicField()
