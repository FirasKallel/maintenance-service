from mongoengine import DynamicField
from .ifc_surface import IfcSurface


class IfcSweptSurface(IfcSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    SweptCurve = DynamicField()
    Position = DynamicField()
