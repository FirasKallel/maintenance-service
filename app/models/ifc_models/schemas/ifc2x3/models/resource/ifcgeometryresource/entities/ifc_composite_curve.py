from mongoengine import DynamicField
from .ifc_bounded_curve import IfcBoundedCurve


class IfcCompositeCurve(IfcBoundedCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    Segments = DynamicField()
    SelfIntersect = DynamicField()
