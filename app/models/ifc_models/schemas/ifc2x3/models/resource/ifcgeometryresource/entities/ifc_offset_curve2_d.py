from mongoengine import DynamicField
from .ifc_curve import IfcCurve


class IfcOffsetCurve2D(IfcCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisCurve = DynamicField()
    Distance = DynamicField()
    SelfIntersect = DynamicField()
