from mongoengine import DynamicField
from .ifc_swept_surface import IfcSweptSurface


class IfcSurfaceOfLinearExtrusion(IfcSweptSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    ExtrudedDirection = DynamicField()
    Depth = DynamicField()
