from mongoengine import DynamicField
from .ifc_cartesian_transformation_operator3_d import IfcCartesianTransformationOperator3D


class IfcCartesianTransformationOperator3DnonUniform(IfcCartesianTransformationOperator3D):
    meta = {
        'namespace': 'ifc2x3'
    }

    Scale2 = DynamicField()
    Scale3 = DynamicField()
