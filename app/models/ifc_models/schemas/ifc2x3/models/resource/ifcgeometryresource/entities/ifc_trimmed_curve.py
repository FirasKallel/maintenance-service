from mongoengine import DynamicField
from .ifc_bounded_curve import IfcBoundedCurve


class IfcTrimmedCurve(IfcBoundedCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisCurve = DynamicField()
    Trim1 = DynamicField()
    Trim2 = DynamicField()
    SenseAgreement = DynamicField()
    MasterRepresentation = DynamicField()
