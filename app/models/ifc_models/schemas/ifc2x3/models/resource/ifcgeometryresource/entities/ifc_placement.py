from mongoengine import DynamicField
from .ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcPlacement(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Location = DynamicField()
