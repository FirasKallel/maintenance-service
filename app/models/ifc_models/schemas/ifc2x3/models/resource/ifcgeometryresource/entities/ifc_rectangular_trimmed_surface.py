from mongoengine import DynamicField
from .ifc_bounded_surface import IfcBoundedSurface


class IfcRectangularTrimmedSurface(IfcBoundedSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisSurface = DynamicField()
    U1 = DynamicField()
    V1 = DynamicField()
    U2 = DynamicField()
    V2 = DynamicField()
    Usense = DynamicField()
    Vsense = DynamicField()
