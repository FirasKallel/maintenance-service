from mongoengine import DynamicField
from .ifc_representation_item import IfcRepresentationItem


class IfcGeometricRepresentationItem(IfcRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

