from mongoengine import DynamicField
from .ifc_placement import IfcPlacement


class IfcAxis2Placement3D(IfcPlacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    Axis = DynamicField()
    RefDirection = DynamicField()
