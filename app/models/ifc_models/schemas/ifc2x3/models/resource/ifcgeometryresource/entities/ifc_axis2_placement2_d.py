from mongoengine import DynamicField
from .ifc_placement import IfcPlacement


class IfcAxis2Placement2D(IfcPlacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    RefDirection = DynamicField()
