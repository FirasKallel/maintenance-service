from mongoengine import DynamicField
from .ifc_bounded_curve import IfcBoundedCurve


class IfcPolyline(IfcBoundedCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    Points = DynamicField()
