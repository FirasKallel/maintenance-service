from mongoengine import DynamicField
from .ifc_swept_surface import IfcSweptSurface


class IfcSurfaceOfRevolution(IfcSweptSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    AxisPosition = DynamicField()
