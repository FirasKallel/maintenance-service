from mongoengine import DynamicField
from .ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcVector(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Orientation = DynamicField()
    Magnitude = DynamicField()
