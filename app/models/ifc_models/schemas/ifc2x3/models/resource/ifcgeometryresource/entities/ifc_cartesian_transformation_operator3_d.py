from mongoengine import DynamicField
from .ifc_cartesian_transformation_operator import IfcCartesianTransformationOperator


class IfcCartesianTransformationOperator3D(IfcCartesianTransformationOperator):
    meta = {
        'namespace': 'ifc2x3'
    }

    Axis3 = DynamicField()
