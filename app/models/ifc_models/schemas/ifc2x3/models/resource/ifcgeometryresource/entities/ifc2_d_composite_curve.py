from mongoengine import DynamicField
from .ifc_composite_curve import IfcCompositeCurve


class Ifc2DCompositeCurve(IfcCompositeCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

