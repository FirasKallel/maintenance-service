from mongoengine import DynamicField
from .ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcDirection(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    DirectionRatios = DynamicField()
