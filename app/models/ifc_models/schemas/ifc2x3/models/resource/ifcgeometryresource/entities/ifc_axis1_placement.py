from mongoengine import DynamicField
from .ifc_placement import IfcPlacement


class IfcAxis1Placement(IfcPlacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    Axis = DynamicField()
