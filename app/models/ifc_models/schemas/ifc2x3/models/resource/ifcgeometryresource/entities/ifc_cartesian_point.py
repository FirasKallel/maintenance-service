from mongoengine import DynamicField
from .ifc_point import IfcPoint


class IfcCartesianPoint(IfcPoint):
    meta = {
        'namespace': 'ifc2x3'
    }

    Coordinates = DynamicField()
