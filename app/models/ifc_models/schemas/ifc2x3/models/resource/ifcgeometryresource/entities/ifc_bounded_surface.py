from mongoengine import DynamicField
from .ifc_surface import IfcSurface


class IfcBoundedSurface(IfcSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

