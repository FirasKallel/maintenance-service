from mongoengine import DynamicField
from .ifc_cartesian_transformation_operator2_d import IfcCartesianTransformationOperator2D


class IfcCartesianTransformationOperator2DnonUniform(IfcCartesianTransformationOperator2D):
    meta = {
        'namespace': 'ifc2x3'
    }

    Scale2 = DynamicField()
