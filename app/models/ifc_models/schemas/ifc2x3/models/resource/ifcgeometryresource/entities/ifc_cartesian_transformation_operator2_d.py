from mongoengine import DynamicField
from .ifc_cartesian_transformation_operator import IfcCartesianTransformationOperator


class IfcCartesianTransformationOperator2D(IfcCartesianTransformationOperator):
    meta = {
        'namespace': 'ifc2x3'
    }

