from mongoengine import DynamicField
from .ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcCartesianTransformationOperator(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Axis1 = DynamicField()
    Axis2 = DynamicField()
    LocalOrigin = DynamicField()
    Scale = DynamicField()
