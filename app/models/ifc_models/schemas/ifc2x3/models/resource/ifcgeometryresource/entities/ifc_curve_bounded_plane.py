from mongoengine import DynamicField
from .ifc_bounded_surface import IfcBoundedSurface


class IfcCurveBoundedPlane(IfcBoundedSurface):
    meta = {
        'namespace': 'ifc2x3'
    }

    BasisSurface = DynamicField()
    OuterBoundary = DynamicField()
    InnerBoundaries = DynamicField()
