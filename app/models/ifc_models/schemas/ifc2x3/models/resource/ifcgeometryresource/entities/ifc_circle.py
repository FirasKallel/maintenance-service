from mongoengine import DynamicField
from .ifc_conic import IfcConic


class IfcCircle(IfcConic):
    meta = {
        'namespace': 'ifc2x3'
    }

    Radius = DynamicField()
