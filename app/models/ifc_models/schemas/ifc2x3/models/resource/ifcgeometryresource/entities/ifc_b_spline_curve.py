from mongoengine import DynamicField
from .ifc_bounded_curve import IfcBoundedCurve


class IfcBSplineCurve(IfcBoundedCurve):
    meta = {
        'namespace': 'ifc2x3'
    }

    Degree = DynamicField()
    ControlPointsList = DynamicField()
    CurveForm = DynamicField()
    ClosedCurve = DynamicField()
    SelfIntersect = DynamicField()
