from mongoengine import DynamicField
from .ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcCompositeCurveSegment(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Transition = DynamicField()
    SameSense = DynamicField()
    ParentCurve = DynamicField()
