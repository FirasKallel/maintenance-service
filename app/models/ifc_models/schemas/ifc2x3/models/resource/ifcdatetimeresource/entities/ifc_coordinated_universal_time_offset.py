from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcCoordinatedUniversalTimeOffset(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    HourOffset = DynamicField()
    MinuteOffset = DynamicField()
    Sense = DynamicField()
