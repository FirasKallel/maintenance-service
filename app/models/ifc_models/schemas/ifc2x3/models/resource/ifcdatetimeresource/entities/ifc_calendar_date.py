from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcCalendarDate(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DayComponent = DynamicField()
    MonthComponent = DynamicField()
    YearComponent = DynamicField()
