from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcLocalTime(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    HourComponent = DynamicField()
    MinuteComponent = DynamicField()
    SecondComponent = DynamicField()
    Zone = DynamicField()
    DaylightSavingOffset = DynamicField()
