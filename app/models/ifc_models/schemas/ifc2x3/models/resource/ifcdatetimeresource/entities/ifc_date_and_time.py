from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDateAndTime(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DateComponent = DynamicField()
    TimeComponent = DynamicField()
