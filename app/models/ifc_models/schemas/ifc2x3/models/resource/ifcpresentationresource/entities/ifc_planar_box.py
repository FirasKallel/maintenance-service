from mongoengine import DynamicField
from .ifc_planar_extent import IfcPlanarExtent


class IfcPlanarBox(IfcPlanarExtent):
    meta = {
        'namespace': 'ifc2x3'
    }

    Placement = DynamicField()
