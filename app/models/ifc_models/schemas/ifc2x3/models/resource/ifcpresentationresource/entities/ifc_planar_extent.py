from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcPlanarExtent(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    SizeInX = DynamicField()
    SizeInY = DynamicField()
