from mongoengine import DynamicField
from .ifc_pre_defined_colour import IfcPreDefinedColour


class IfcDraughtingPreDefinedColour(IfcPreDefinedColour):
    meta = {
        'namespace': 'ifc2x3'
    }

