from mongoengine import DynamicField
from ...ifcexternalreferenceresource.entities.ifc_external_reference import IfcExternalReference


class IfcExternallyDefinedTextFont(IfcExternalReference):
    meta = {
        'namespace': 'ifc2x3'
    }

