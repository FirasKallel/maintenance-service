from mongoengine import DynamicField
from .ifc_pre_defined_item import IfcPreDefinedItem


class IfcPreDefinedTextFont(IfcPreDefinedItem):
    meta = {
        'namespace': 'ifc2x3'
    }

