from mongoengine import DynamicField
from .ifc_colour_specification import IfcColourSpecification


class IfcColourRgb(IfcColourSpecification):
    meta = {
        'namespace': 'ifc2x3'
    }

    Red = DynamicField()
    Green = DynamicField()
    Blue = DynamicField()
