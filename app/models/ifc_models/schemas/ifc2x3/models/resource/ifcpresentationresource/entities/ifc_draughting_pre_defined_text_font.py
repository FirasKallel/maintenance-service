from mongoengine import DynamicField
from .ifc_pre_defined_text_font import IfcPreDefinedTextFont


class IfcDraughtingPreDefinedTextFont(IfcPreDefinedTextFont):
    meta = {
        'namespace': 'ifc2x3'
    }

