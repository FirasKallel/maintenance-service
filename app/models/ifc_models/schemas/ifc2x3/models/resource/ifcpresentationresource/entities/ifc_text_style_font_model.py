from mongoengine import DynamicField
from .ifc_pre_defined_text_font import IfcPreDefinedTextFont


class IfcTextStyleFontModel(IfcPreDefinedTextFont):
    meta = {
        'namespace': 'ifc2x3'
    }

    FontFamily = DynamicField()
    FontStyle = DynamicField()
    FontVariant = DynamicField()
    FontWeight = DynamicField()
    FontSize = DynamicField()
