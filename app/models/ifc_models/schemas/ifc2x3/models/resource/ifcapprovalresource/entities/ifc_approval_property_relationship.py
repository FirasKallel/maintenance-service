from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcApprovalPropertyRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ApprovedProperties = DynamicField()
    Approval = DynamicField()
