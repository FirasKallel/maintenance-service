from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcApproval(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Description = DynamicField()
    ApprovalDateTime = DynamicField()
    ApprovalStatus = DynamicField()
    ApprovalLevel = DynamicField()
    ApprovalQualifier = DynamicField()
    Name = DynamicField()
    Identifier = DynamicField()
