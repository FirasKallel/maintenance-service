from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcApprovalActorRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Actor = DynamicField()
    Approval = DynamicField()
    Role = DynamicField()
