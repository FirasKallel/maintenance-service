from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcApprovalRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatedApproval = DynamicField()
    RelatingApproval = DynamicField()
    Description = DynamicField()
    Name = DynamicField()
