from mongoengine import DynamicField
from .ifc_structural_load_single_displacement import IfcStructuralLoadSingleDisplacement


class IfcStructuralLoadSingleDisplacementDistortion(IfcStructuralLoadSingleDisplacement):
    meta = {
        'namespace': 'ifc2x3'
    }

    Distortion = DynamicField()
