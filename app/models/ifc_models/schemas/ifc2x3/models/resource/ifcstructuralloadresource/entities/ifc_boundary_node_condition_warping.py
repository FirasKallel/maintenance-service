from mongoengine import DynamicField
from .ifc_boundary_node_condition import IfcBoundaryNodeCondition


class IfcBoundaryNodeConditionWarping(IfcBoundaryNodeCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    WarpingStiffness = DynamicField()
