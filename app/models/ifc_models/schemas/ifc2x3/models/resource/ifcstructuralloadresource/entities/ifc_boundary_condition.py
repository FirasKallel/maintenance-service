from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcBoundaryCondition(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
