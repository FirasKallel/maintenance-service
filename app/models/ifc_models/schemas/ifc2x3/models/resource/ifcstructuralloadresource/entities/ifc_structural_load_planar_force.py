from mongoengine import DynamicField
from .ifc_structural_load_static import IfcStructuralLoadStatic


class IfcStructuralLoadPlanarForce(IfcStructuralLoadStatic):
    meta = {
        'namespace': 'ifc2x3'
    }

    PlanarForceX = DynamicField()
    PlanarForceY = DynamicField()
    PlanarForceZ = DynamicField()
