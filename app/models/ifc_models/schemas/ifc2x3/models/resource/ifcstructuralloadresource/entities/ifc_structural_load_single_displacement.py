from mongoengine import DynamicField
from .ifc_structural_load_static import IfcStructuralLoadStatic


class IfcStructuralLoadSingleDisplacement(IfcStructuralLoadStatic):
    meta = {
        'namespace': 'ifc2x3'
    }

    DisplacementX = DynamicField()
    DisplacementY = DynamicField()
    DisplacementZ = DynamicField()
    RotationalDisplacementRX = DynamicField()
    RotationalDisplacementRY = DynamicField()
    RotationalDisplacementRZ = DynamicField()
