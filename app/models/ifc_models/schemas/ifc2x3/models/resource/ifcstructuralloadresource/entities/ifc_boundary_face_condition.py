from mongoengine import DynamicField
from .ifc_boundary_condition import IfcBoundaryCondition


class IfcBoundaryFaceCondition(IfcBoundaryCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    LinearStiffnessByAreaX = DynamicField()
    LinearStiffnessByAreaY = DynamicField()
    LinearStiffnessByAreaZ = DynamicField()
