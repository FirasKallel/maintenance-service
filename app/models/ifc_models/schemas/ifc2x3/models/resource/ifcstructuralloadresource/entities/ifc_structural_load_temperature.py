from mongoengine import DynamicField
from .ifc_structural_load_static import IfcStructuralLoadStatic


class IfcStructuralLoadTemperature(IfcStructuralLoadStatic):
    meta = {
        'namespace': 'ifc2x3'
    }

    DeltaT_Constant = DynamicField()
    DeltaT_Y = DynamicField()
    DeltaT_Z = DynamicField()
