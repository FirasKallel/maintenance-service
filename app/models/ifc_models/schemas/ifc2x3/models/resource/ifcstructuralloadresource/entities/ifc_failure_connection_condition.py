from mongoengine import DynamicField
from .ifc_structural_connection_condition import IfcStructuralConnectionCondition


class IfcFailureConnectionCondition(IfcStructuralConnectionCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    TensionFailureX = DynamicField()
    TensionFailureY = DynamicField()
    TensionFailureZ = DynamicField()
    CompressionFailureX = DynamicField()
    CompressionFailureY = DynamicField()
    CompressionFailureZ = DynamicField()
