from mongoengine import DynamicField
from .ifc_structural_load_static import IfcStructuralLoadStatic


class IfcStructuralLoadLinearForce(IfcStructuralLoadStatic):
    meta = {
        'namespace': 'ifc2x3'
    }

    LinearForceX = DynamicField()
    LinearForceY = DynamicField()
    LinearForceZ = DynamicField()
    LinearMomentX = DynamicField()
    LinearMomentY = DynamicField()
    LinearMomentZ = DynamicField()
