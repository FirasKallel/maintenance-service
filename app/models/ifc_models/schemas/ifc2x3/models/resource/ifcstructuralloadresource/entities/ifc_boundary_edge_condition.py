from mongoengine import DynamicField
from .ifc_boundary_condition import IfcBoundaryCondition


class IfcBoundaryEdgeCondition(IfcBoundaryCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    LinearStiffnessByLengthX = DynamicField()
    LinearStiffnessByLengthY = DynamicField()
    LinearStiffnessByLengthZ = DynamicField()
    RotationalStiffnessByLengthX = DynamicField()
    RotationalStiffnessByLengthY = DynamicField()
    RotationalStiffnessByLengthZ = DynamicField()
