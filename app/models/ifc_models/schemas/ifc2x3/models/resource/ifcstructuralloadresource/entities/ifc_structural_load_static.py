from mongoengine import DynamicField
from .ifc_structural_load import IfcStructuralLoad


class IfcStructuralLoadStatic(IfcStructuralLoad):
    meta = {
        'namespace': 'ifc2x3'
    }

