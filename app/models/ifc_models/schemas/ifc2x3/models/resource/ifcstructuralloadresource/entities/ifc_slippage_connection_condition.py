from mongoengine import DynamicField
from .ifc_structural_connection_condition import IfcStructuralConnectionCondition


class IfcSlippageConnectionCondition(IfcStructuralConnectionCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    SlippageX = DynamicField()
    SlippageY = DynamicField()
    SlippageZ = DynamicField()
