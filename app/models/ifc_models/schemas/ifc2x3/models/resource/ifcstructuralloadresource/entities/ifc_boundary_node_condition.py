from mongoengine import DynamicField
from .ifc_boundary_condition import IfcBoundaryCondition


class IfcBoundaryNodeCondition(IfcBoundaryCondition):
    meta = {
        'namespace': 'ifc2x3'
    }

    LinearStiffnessX = DynamicField()
    LinearStiffnessY = DynamicField()
    LinearStiffnessZ = DynamicField()
    RotationalStiffnessX = DynamicField()
    RotationalStiffnessY = DynamicField()
    RotationalStiffnessZ = DynamicField()
