from mongoengine import DynamicField
from .ifc_structural_load_static import IfcStructuralLoadStatic


class IfcStructuralLoadSingleForce(IfcStructuralLoadStatic):
    meta = {
        'namespace': 'ifc2x3'
    }

    ForceX = DynamicField()
    ForceY = DynamicField()
    ForceZ = DynamicField()
    MomentX = DynamicField()
    MomentY = DynamicField()
    MomentZ = DynamicField()
