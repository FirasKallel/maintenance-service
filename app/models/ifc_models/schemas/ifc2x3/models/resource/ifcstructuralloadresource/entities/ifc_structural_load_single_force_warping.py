from mongoengine import DynamicField
from .ifc_structural_load_single_force import IfcStructuralLoadSingleForce


class IfcStructuralLoadSingleForceWarping(IfcStructuralLoadSingleForce):
    meta = {
        'namespace': 'ifc2x3'
    }

    WarpingMoment = DynamicField()
