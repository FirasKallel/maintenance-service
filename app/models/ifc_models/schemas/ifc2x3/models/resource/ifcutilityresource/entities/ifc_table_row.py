from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTableRow(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RowCells = DynamicField()
    IsHeading = DynamicField()
