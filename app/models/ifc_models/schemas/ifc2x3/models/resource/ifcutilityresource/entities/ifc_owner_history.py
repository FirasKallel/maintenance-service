from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcOwnerHistory(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    OwningUser = DynamicField()
    OwningApplication = DynamicField()
    State = DynamicField()
    ChangeAction = DynamicField()
    LastModifiedDate = DynamicField()
    LastModifyingUser = DynamicField()
    LastModifyingApplication = DynamicField()
    CreationDate = DynamicField()
