from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcApplication(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ApplicationDeveloper = DynamicField()
    Version = DynamicField()
    ApplicationFullName = DynamicField()
    ApplicationIdentifier = DynamicField()
