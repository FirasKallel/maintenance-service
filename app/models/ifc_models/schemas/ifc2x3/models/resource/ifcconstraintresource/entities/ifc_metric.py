from mongoengine import DynamicField
from .ifc_constraint import IfcConstraint


class IfcMetric(IfcConstraint):
    meta = {
        'namespace': 'ifc2x3'
    }

    Benchmark = DynamicField()
    ValueSource = DynamicField()
    DataValue = DynamicField()
