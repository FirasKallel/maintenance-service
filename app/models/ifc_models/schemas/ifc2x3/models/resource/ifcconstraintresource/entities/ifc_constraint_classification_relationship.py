from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcConstraintClassificationRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ClassifiedConstraint = DynamicField()
    RelatedClassifications = DynamicField()
