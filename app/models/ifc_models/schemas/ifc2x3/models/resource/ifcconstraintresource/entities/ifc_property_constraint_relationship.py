from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPropertyConstraintRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingConstraint = DynamicField()
    RelatedProperties = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
