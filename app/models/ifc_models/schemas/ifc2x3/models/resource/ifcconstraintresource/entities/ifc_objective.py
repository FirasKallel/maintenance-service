from mongoengine import DynamicField
from .ifc_constraint import IfcConstraint


class IfcObjective(IfcConstraint):
    meta = {
        'namespace': 'ifc2x3'
    }

    BenchmarkValues = DynamicField()
    ResultValues = DynamicField()
    ObjectiveQualifier = DynamicField()
    UserDefinedQualifier = DynamicField()
