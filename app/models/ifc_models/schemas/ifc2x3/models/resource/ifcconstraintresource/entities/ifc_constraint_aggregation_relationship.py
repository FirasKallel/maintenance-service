from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcConstraintAggregationRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    RelatingConstraint = DynamicField()
    RelatedConstraints = DynamicField()
    LogicalAggregator = DynamicField()
