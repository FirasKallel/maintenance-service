from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcConstraint(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    ConstraintGrade = DynamicField()
    ConstraintSource = DynamicField()
    CreatingActor = DynamicField()
    CreationTime = DynamicField()
    UserDefinedGrade = DynamicField()
