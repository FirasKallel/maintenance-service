from mongoengine import DynamicField
from .ifc_surface_style_shading import IfcSurfaceStyleShading


class IfcSurfaceStyleRendering(IfcSurfaceStyleShading):
    meta = {
        'namespace': 'ifc2x3'
    }

    Transparency = DynamicField()
    DiffuseColour = DynamicField()
    TransmissionColour = DynamicField()
    DiffuseTransmissionColour = DynamicField()
    ReflectionColour = DynamicField()
    SpecularColour = DynamicField()
    SpecularHighlight = DynamicField()
    ReflectanceMethod = DynamicField()
