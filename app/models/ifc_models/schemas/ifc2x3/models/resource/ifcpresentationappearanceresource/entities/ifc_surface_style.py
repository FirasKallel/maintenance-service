from mongoengine import DynamicField
from .ifc_presentation_style import IfcPresentationStyle


class IfcSurfaceStyle(IfcPresentationStyle):
    meta = {
        'namespace': 'ifc2x3'
    }

    Side = DynamicField()
    Styles = DynamicField()
