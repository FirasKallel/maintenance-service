from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSurfaceStyleWithTextures(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Textures = DynamicField()
