from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSurfaceStyleShading(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    SurfaceColour = DynamicField()
