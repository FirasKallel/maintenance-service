from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSurfaceStyleRefraction(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RefractionIndex = DynamicField()
    DispersionFactor = DynamicField()
