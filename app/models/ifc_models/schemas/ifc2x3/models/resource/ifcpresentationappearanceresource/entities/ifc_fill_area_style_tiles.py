from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcFillAreaStyleTiles(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    TilingPattern = DynamicField()
    Tiles = DynamicField()
    TilingScale = DynamicField()
