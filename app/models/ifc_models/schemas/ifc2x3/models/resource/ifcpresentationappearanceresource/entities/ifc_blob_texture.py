from mongoengine import DynamicField
from .ifc_surface_texture import IfcSurfaceTexture


class IfcBlobTexture(IfcSurfaceTexture):
    meta = {
        'namespace': 'ifc2x3'
    }

    RasterFormat = DynamicField()
    RasterCode = DynamicField()
