from mongoengine import DynamicField
from ...ifcexternalreferenceresource.entities.ifc_external_reference import IfcExternalReference


class IfcExternallyDefinedSurfaceStyle(IfcExternalReference):
    meta = {
        'namespace': 'ifc2x3'
    }

