from mongoengine import DynamicField
from .ifc_presentation_style import IfcPresentationStyle


class IfcSymbolStyle(IfcPresentationStyle):
    meta = {
        'namespace': 'ifc2x3'
    }

    StyleOfSymbol = DynamicField()
