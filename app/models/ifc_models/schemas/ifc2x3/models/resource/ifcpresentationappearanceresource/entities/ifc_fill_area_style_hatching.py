from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcFillAreaStyleHatching(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    HatchLineAppearance = DynamicField()
    StartOfNextHatchLine = DynamicField()
    PointOfReferenceHatchLine = DynamicField()
    PatternStart = DynamicField()
    HatchLineAngle = DynamicField()
