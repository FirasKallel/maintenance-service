from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTextStyleForDefinedFont(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Colour = DynamicField()
    BackgroundColour = DynamicField()
