from mongoengine import DynamicField
from .ifc_surface_texture import IfcSurfaceTexture


class IfcImageTexture(IfcSurfaceTexture):
    meta = {
        'namespace': 'ifc2x3'
    }

    UrlReference = DynamicField()
