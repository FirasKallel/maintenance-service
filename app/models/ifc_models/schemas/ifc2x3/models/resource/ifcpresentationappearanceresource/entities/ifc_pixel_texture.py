from mongoengine import DynamicField
from .ifc_surface_texture import IfcSurfaceTexture


class IfcPixelTexture(IfcSurfaceTexture):
    meta = {
        'namespace': 'ifc2x3'
    }

    Width = DynamicField()
    Height = DynamicField()
    ColourComponents = DynamicField()
    Pixel = DynamicField()
