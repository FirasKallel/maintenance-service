from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTextStyleTextModel(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    TextIndent = DynamicField()
    TextAlign = DynamicField()
    TextDecoration = DynamicField()
    LetterSpacing = DynamicField()
    WordSpacing = DynamicField()
    TextTransform = DynamicField()
    LineHeight = DynamicField()
