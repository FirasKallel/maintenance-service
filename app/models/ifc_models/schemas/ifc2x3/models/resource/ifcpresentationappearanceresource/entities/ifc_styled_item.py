from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_representation_item import IfcRepresentationItem


class IfcStyledItem(IfcRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Item = DynamicField()
    Styles = DynamicField()
    Name = DynamicField()
