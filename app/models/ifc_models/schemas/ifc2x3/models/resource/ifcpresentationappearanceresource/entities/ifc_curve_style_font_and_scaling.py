from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcCurveStyleFontAndScaling(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    CurveFont = DynamicField()
    CurveFontScaling = DynamicField()
