from mongoengine import DynamicField
from .ifc_one_direction_repeat_factor import IfcOneDirectionRepeatFactor


class IfcTwoDirectionRepeatFactor(IfcOneDirectionRepeatFactor):
    meta = {
        'namespace': 'ifc2x3'
    }

    SecondRepeatFactor = DynamicField()
