from mongoengine import DynamicField
from .ifc_presentation_style import IfcPresentationStyle


class IfcTextStyle(IfcPresentationStyle):
    meta = {
        'namespace': 'ifc2x3'
    }

    TextCharacterAppearance = DynamicField()
    TextStyle = DynamicField()
    TextFontStyle = DynamicField()
