from mongoengine import DynamicField
from .ifc_presentation_style import IfcPresentationStyle


class IfcCurveStyle(IfcPresentationStyle):
    meta = {
        'namespace': 'ifc2x3'
    }

    CurveFont = DynamicField()
    CurveWidth = DynamicField()
    CurveColour = DynamicField()
