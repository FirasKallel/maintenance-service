from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPresentationStyleAssignment(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Styles = DynamicField()
