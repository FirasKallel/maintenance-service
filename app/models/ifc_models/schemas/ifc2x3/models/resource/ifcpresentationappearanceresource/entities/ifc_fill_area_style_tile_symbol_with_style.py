from mongoengine import DynamicField
from ...ifcgeometryresource.entities.ifc_geometric_representation_item import IfcGeometricRepresentationItem


class IfcFillAreaStyleTileSymbolWithStyle(IfcGeometricRepresentationItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    Symbol = DynamicField()
