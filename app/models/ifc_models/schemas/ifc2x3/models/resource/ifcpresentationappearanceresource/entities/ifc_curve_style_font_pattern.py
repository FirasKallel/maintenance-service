from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcCurveStyleFontPattern(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    VisibleSegmentLength = DynamicField()
    InvisibleSegmentLength = DynamicField()
