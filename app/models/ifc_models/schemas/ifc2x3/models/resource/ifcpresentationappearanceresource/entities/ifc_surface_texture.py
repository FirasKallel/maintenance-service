from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSurfaceTexture(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RepeatS = DynamicField()
    RepeatT = DynamicField()
    TextureType = DynamicField()
    TextureTransform = DynamicField()
