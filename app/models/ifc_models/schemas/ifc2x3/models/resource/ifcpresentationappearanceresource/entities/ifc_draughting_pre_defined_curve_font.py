from mongoengine import DynamicField
from .ifc_pre_defined_curve_font import IfcPreDefinedCurveFont


class IfcDraughtingPreDefinedCurveFont(IfcPreDefinedCurveFont):
    meta = {
        'namespace': 'ifc2x3'
    }

