from mongoengine import DynamicField
from ...ifcpresentationresource.entities.ifc_pre_defined_item import IfcPreDefinedItem


class IfcPreDefinedCurveFont(IfcPreDefinedItem):
    meta = {
        'namespace': 'ifc2x3'
    }

