from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTextStyleWithBoxCharacteristics(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    BoxHeight = DynamicField()
    BoxWidth = DynamicField()
    BoxSlantAngle = DynamicField()
    BoxRotateAngle = DynamicField()
    CharacterSpacing = DynamicField()
