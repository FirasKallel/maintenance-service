from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcPresentationStyle(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
