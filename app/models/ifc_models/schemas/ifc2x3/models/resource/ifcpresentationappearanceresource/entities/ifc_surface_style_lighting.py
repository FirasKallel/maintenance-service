from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcSurfaceStyleLighting(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DiffuseTransmissionColour = DynamicField()
    DiffuseReflectionColour = DynamicField()
    TransmissionColour = DynamicField()
    ReflectanceColour = DynamicField()
