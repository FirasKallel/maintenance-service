from mongoengine import DynamicField
from .ifc_presentation_style import IfcPresentationStyle


class IfcFillAreaStyle(IfcPresentationStyle):
    meta = {
        'namespace': 'ifc2x3'
    }

    FillStyles = DynamicField()
