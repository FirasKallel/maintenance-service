from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTimeSeries(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Description = DynamicField()
    StartTime = DynamicField()
    EndTime = DynamicField()
    TimeSeriesDataType = DynamicField()
    DataOrigin = DynamicField()
    UserDefinedDataOrigin = DynamicField()
    Unit = DynamicField()
