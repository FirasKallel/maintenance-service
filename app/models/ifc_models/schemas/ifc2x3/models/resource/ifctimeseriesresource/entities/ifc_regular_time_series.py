from mongoengine import DynamicField
from .ifc_time_series import IfcTimeSeries


class IfcRegularTimeSeries(IfcTimeSeries):
    meta = {
        'namespace': 'ifc2x3'
    }

    TimeStep = DynamicField()
    Values = DynamicField()
