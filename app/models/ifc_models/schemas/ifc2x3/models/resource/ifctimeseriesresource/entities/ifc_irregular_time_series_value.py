from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcIrregularTimeSeriesValue(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    TimeStamp = DynamicField()
    ListValues = DynamicField()
