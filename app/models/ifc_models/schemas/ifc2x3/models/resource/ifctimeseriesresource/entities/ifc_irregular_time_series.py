from mongoengine import DynamicField
from .ifc_time_series import IfcTimeSeries


class IfcIrregularTimeSeries(IfcTimeSeries):
    meta = {
        'namespace': 'ifc2x3'
    }

    Values = DynamicField()
