from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcTimeSeriesValue(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ListValues = DynamicField()
