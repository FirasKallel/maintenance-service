from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcClassificationItemRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingItem = DynamicField()
    RelatedItems = DynamicField()
