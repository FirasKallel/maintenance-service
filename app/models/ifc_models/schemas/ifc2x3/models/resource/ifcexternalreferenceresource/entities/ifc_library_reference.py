from mongoengine import DynamicField
from .ifc_external_reference import IfcExternalReference


class IfcLibraryReference(IfcExternalReference):
    meta = {
        'namespace': 'ifc2x3'
    }

