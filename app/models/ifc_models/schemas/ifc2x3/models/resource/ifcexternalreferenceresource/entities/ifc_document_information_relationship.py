from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDocumentInformationRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingDocument = DynamicField()
    RelatedDocuments = DynamicField()
    RelationshipType = DynamicField()
