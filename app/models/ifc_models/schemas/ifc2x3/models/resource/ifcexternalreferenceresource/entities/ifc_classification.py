from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcClassification(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Source = DynamicField()
    Edition = DynamicField()
    EditionDate = DynamicField()
    Name = DynamicField()
