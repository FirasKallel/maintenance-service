from mongoengine import DynamicField
from .ifc_external_reference import IfcExternalReference


class IfcClassificationReference(IfcExternalReference):
    meta = {
        'namespace': 'ifc2x3'
    }

    ReferencedSource = DynamicField()
