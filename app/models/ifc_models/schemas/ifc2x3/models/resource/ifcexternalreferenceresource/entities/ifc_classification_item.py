from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcClassificationItem(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Notation = DynamicField()
    ItemOf = DynamicField()
    Title = DynamicField()
