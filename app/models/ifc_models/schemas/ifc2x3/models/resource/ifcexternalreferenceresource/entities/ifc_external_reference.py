from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcExternalReference(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Location = DynamicField()
    ItemReference = DynamicField()
    Name = DynamicField()
