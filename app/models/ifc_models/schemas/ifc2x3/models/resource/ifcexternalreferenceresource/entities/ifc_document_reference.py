from mongoengine import DynamicField
from .ifc_external_reference import IfcExternalReference


class IfcDocumentReference(IfcExternalReference):
    meta = {
        'namespace': 'ifc2x3'
    }

