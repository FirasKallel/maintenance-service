from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDocumentElectronicFormat(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    FileExtension = DynamicField()
    MimeContentType = DynamicField()
    MimeSubtype = DynamicField()
