from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcClassificationNotationFacet(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    NotationValue = DynamicField()
