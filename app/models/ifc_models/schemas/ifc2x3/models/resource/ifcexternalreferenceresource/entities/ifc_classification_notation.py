from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcClassificationNotation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    NotationFacets = DynamicField()
