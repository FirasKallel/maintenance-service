from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDocumentInformation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DocumentId = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
    DocumentReferences = DynamicField()
    Purpose = DynamicField()
    IntendedUse = DynamicField()
    Scope = DynamicField()
    Revision = DynamicField()
    DocumentOwner = DynamicField()
    Editors = DynamicField()
    CreationTime = DynamicField()
    LastRevisionTime = DynamicField()
    ElectronicFormat = DynamicField()
    ValidFrom = DynamicField()
    ValidUntil = DynamicField()
    Confidentiality = DynamicField()
    Status = DynamicField()
