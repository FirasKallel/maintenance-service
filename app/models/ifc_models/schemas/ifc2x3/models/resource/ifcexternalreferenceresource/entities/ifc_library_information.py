from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcLibraryInformation(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
    Version = DynamicField()
    Publisher = DynamicField()
    VersionDate = DynamicField()
    LibraryReference = DynamicField()
