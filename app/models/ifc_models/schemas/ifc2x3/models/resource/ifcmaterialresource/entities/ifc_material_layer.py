from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialLayer(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Material = DynamicField()
    LayerThickness = DynamicField()
    IsVentilated = DynamicField()
