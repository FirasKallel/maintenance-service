from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialList(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Materials = DynamicField()
