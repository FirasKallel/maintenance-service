from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialLayerSet(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    MaterialLayers = DynamicField()
    LayerSetName = DynamicField()
