from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialClassificationRelationship(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    MaterialClassifications = DynamicField()
    ClassifiedMaterial = DynamicField()
