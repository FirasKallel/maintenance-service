from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMaterialLayerSetUsage(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ForLayerSet = DynamicField()
    LayerSetDirection = DynamicField()
    DirectionSense = DynamicField()
    OffsetFromReferenceLine = DynamicField()
