from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMonetaryUnit(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Currency = DynamicField()
