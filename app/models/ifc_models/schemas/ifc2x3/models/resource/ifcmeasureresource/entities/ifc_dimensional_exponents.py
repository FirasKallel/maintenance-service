from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDimensionalExponents(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    LengthExponent = DynamicField()
    MassExponent = DynamicField()
    TimeExponent = DynamicField()
    ElectricCurrentExponent = DynamicField()
    ThermodynamicTemperatureExponent = DynamicField()
    AmountOfSubstanceExponent = DynamicField()
    LuminousIntensityExponent = DynamicField()
