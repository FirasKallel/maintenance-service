from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcMeasureWithUnit(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    ValueComponent = DynamicField()
    UnitComponent = DynamicField()
