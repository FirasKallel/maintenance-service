from mongoengine import DynamicField
from .ifc_named_unit import IfcNamedUnit


class IfcContextDependentUnit(IfcNamedUnit):
    meta = {
        'namespace': 'ifc2x3'
    }

    Name = DynamicField()
