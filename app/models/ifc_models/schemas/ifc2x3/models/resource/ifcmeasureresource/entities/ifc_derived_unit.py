from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDerivedUnit(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Elements = DynamicField()
    UnitType = DynamicField()
    UserDefinedType = DynamicField()
