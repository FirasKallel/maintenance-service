from mongoengine import DynamicField
from .ifc_named_unit import IfcNamedUnit


class IfcSIUnit(IfcNamedUnit):
    meta = {
        'namespace': 'ifc2x3'
    }

    Prefix = DynamicField()
    Name = DynamicField()
