from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcDerivedUnitElement(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Unit = DynamicField()
    Exponent = DynamicField()
