from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcNamedUnit(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Dimensions = DynamicField()
    UnitType = DynamicField()
