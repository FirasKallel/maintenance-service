from mongoengine import DynamicField
from .......common.ifc_base import IfcEntity


class IfcUnitAssignment(IfcEntity):
    meta = {
        'namespace': 'ifc2x3'
    }

    Units = DynamicField()
