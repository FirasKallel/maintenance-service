from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcSpaceProgram(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    SpaceProgramIdentifier = DynamicField()
    MaxRequiredArea = DynamicField()
    MinRequiredArea = DynamicField()
    RequestedLocation = DynamicField()
    StandardRequiredArea = DynamicField()
