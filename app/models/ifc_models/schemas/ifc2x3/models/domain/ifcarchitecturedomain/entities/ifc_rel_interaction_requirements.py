from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelInteractionRequirements(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    DailyInteraction = DynamicField()
    ImportanceRating = DynamicField()
    LocationOfInteraction = DynamicField()
    RelatedSpaceProgram = DynamicField()
    RelatingSpaceProgram = DynamicField()
