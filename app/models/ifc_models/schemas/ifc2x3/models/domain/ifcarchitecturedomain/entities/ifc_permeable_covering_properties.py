from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcPermeableCoveringProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    OperationType = DynamicField()
    PanelPosition = DynamicField()
    FrameDepth = DynamicField()
    FrameThickness = DynamicField()
    ShapeAspectStyle = DynamicField()
