from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_fitting_type import IfcFlowFittingType


class IfcPipeFittingType(IfcFlowFittingType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
