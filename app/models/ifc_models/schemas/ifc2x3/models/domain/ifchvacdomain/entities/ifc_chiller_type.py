from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_energy_conversion_device_type import IfcEnergyConversionDeviceType


class IfcChillerType(IfcEnergyConversionDeviceType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
