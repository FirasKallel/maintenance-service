from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_treatment_device_type import IfcFlowTreatmentDeviceType


class IfcDuctSilencerType(IfcFlowTreatmentDeviceType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
