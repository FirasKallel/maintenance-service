from mongoengine import DynamicField
from ....shared.ifcsharedcomponentelements.entities.ifc_discrete_accessory_type import IfcDiscreteAccessoryType


class IfcVibrationIsolatorType(IfcDiscreteAccessoryType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
