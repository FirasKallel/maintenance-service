from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_segment_type import IfcFlowSegmentType


class IfcDuctSegmentType(IfcFlowSegmentType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
