from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_moving_device_type import IfcFlowMovingDeviceType


class IfcCompressorType(IfcFlowMovingDeviceType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
