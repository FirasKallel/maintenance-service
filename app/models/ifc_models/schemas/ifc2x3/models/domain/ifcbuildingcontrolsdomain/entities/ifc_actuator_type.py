from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_distribution_control_element_type import IfcDistributionControlElementType


class IfcActuatorType(IfcDistributionControlElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
