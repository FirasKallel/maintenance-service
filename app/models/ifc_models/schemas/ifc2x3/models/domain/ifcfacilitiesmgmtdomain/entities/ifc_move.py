from mongoengine import DynamicField
from ....core.ifcprocessextension.entities.ifc_task import IfcTask


class IfcMove(IfcTask):
    meta = {
        'namespace': 'ifc2x3'
    }

    MoveFrom = DynamicField()
    MoveTo = DynamicField()
    PunchList = DynamicField()
