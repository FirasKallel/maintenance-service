from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcFurnitureStandard(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

