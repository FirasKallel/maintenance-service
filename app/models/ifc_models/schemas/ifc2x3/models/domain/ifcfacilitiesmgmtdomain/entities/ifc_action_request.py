from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcActionRequest(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    RequestID = DynamicField()
