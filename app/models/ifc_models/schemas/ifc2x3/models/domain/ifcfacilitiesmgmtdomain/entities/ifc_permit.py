from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcPermit(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    PermitID = DynamicField()
