from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_group import IfcGroup


class IfcCondition(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

