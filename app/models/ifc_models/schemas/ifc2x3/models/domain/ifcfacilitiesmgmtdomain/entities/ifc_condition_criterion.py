from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_control import IfcControl


class IfcConditionCriterion(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    Criterion = DynamicField()
    CriterionDateTime = DynamicField()
