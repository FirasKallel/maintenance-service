from mongoengine import DynamicField
from ....core.ifcprocessextension.entities.ifc_task import IfcTask


class IfcOrderAction(IfcTask):
    meta = {
        'namespace': 'ifc2x3'
    }

    ActionID = DynamicField()
