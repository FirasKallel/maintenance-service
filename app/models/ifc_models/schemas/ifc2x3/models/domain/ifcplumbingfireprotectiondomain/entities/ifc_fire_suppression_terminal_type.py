from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_terminal_type import IfcFlowTerminalType


class IfcFireSuppressionTerminalType(IfcFlowTerminalType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
