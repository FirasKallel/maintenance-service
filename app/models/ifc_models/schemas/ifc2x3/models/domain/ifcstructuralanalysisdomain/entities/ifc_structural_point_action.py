from mongoengine import DynamicField
from .ifc_structural_action import IfcStructuralAction


class IfcStructuralPointAction(IfcStructuralAction):
    meta = {
        'namespace': 'ifc2x3'
    }

