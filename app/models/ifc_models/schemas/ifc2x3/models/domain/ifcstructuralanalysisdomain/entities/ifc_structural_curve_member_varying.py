from mongoengine import DynamicField
from .ifc_structural_curve_member import IfcStructuralCurveMember


class IfcStructuralCurveMemberVarying(IfcStructuralCurveMember):
    meta = {
        'namespace': 'ifc2x3'
    }

