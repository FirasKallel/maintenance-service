from mongoengine import DynamicField
from .ifc_structural_activity import IfcStructuralActivity


class IfcStructuralReaction(IfcStructuralActivity):
    meta = {
        'namespace': 'ifc2x3'
    }

