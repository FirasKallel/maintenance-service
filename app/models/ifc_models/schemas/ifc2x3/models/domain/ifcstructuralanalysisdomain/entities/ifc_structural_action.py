from mongoengine import DynamicField
from .ifc_structural_activity import IfcStructuralActivity


class IfcStructuralAction(IfcStructuralActivity):
    meta = {
        'namespace': 'ifc2x3'
    }

    DestabilizingLoad = DynamicField()
    CausedBy = DynamicField()
