from mongoengine import DynamicField
from .ifc_structural_linear_action import IfcStructuralLinearAction


class IfcStructuralLinearActionVarying(IfcStructuralLinearAction):
    meta = {
        'namespace': 'ifc2x3'
    }

    VaryingAppliedLoadLocation = DynamicField()
    SubsequentAppliedLoads = DynamicField()
