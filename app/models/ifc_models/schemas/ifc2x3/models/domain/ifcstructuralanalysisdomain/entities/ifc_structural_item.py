from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_product import IfcProduct


class IfcStructuralItem(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

