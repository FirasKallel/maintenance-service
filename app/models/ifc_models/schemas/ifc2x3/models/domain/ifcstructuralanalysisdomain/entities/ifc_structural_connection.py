from mongoengine import DynamicField
from .ifc_structural_item import IfcStructuralItem


class IfcStructuralConnection(IfcStructuralItem):
    meta = {
        'namespace': 'ifc2x3'
    }

    AppliedCondition = DynamicField()
