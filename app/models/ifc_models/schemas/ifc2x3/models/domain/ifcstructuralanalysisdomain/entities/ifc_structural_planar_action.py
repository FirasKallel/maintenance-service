from mongoengine import DynamicField
from .ifc_structural_action import IfcStructuralAction


class IfcStructuralPlanarAction(IfcStructuralAction):
    meta = {
        'namespace': 'ifc2x3'
    }

    ProjectedOrTrue = DynamicField()
