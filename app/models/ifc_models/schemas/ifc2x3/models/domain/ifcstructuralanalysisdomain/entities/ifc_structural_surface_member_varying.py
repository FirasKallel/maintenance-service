from mongoengine import DynamicField
from .ifc_structural_surface_member import IfcStructuralSurfaceMember


class IfcStructuralSurfaceMemberVarying(IfcStructuralSurfaceMember):
    meta = {
        'namespace': 'ifc2x3'
    }

    SubsequentThickness = DynamicField()
    VaryingThicknessLocation = DynamicField()
