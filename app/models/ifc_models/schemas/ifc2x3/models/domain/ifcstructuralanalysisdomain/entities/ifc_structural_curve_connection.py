from mongoengine import DynamicField
from .ifc_structural_connection import IfcStructuralConnection


class IfcStructuralCurveConnection(IfcStructuralConnection):
    meta = {
        'namespace': 'ifc2x3'
    }

