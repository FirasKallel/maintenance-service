from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_product import IfcProduct


class IfcStructuralActivity(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    AppliedLoad = DynamicField()
    GlobalOrLocal = DynamicField()
