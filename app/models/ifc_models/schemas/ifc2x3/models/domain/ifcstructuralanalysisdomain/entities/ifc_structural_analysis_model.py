from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_system import IfcSystem


class IfcStructuralAnalysisModel(IfcSystem):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
    OrientationOf2DPlane = DynamicField()
    LoadedBy = DynamicField()
    HasResults = DynamicField()
