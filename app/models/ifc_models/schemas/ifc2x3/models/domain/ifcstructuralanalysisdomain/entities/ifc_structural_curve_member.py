from mongoengine import DynamicField
from .ifc_structural_member import IfcStructuralMember


class IfcStructuralCurveMember(IfcStructuralMember):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
