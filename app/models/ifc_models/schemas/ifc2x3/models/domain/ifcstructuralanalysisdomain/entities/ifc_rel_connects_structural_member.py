from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelConnectsStructuralMember(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingStructuralMember = DynamicField()
    RelatedStructuralConnection = DynamicField()
    AppliedCondition = DynamicField()
    AdditionalConditions = DynamicField()
    SupportedLength = DynamicField()
    ConditionCoordinateSystem = DynamicField()
