from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_group import IfcGroup


class IfcStructuralLoadGroup(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
    ActionType = DynamicField()
    ActionSource = DynamicField()
    Coefficient = DynamicField()
    Purpose = DynamicField()
