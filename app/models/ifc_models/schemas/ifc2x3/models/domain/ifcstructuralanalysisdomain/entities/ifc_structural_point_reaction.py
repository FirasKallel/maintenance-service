from mongoengine import DynamicField
from .ifc_structural_reaction import IfcStructuralReaction


class IfcStructuralPointReaction(IfcStructuralReaction):
    meta = {
        'namespace': 'ifc2x3'
    }

