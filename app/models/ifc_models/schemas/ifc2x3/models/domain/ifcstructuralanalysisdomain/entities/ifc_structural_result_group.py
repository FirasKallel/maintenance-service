from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_group import IfcGroup


class IfcStructuralResultGroup(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

    TheoryType = DynamicField()
    ResultForLoadGroup = DynamicField()
    IsLinear = DynamicField()
