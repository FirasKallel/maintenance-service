from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelConnectsStructuralActivity(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingElement = DynamicField()
    RelatedStructuralActivity = DynamicField()
