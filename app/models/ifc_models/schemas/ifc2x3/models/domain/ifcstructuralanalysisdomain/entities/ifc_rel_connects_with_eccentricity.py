from mongoengine import DynamicField
from .ifc_rel_connects_structural_member import IfcRelConnectsStructuralMember


class IfcRelConnectsWithEccentricity(IfcRelConnectsStructuralMember):
    meta = {
        'namespace': 'ifc2x3'
    }

    ConnectionConstraint = DynamicField()
