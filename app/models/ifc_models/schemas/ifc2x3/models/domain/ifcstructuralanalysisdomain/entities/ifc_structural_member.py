from mongoengine import DynamicField
from .ifc_structural_item import IfcStructuralItem


class IfcStructuralMember(IfcStructuralItem):
    meta = {
        'namespace': 'ifc2x3'
    }

