from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesProfileProperties(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingProfileProperties = DynamicField()
    ProfileSectionLocation = DynamicField()
    ProfileOrientation = DynamicField()
