from mongoengine import DynamicField
from .ifc_structural_planar_action import IfcStructuralPlanarAction


class IfcStructuralPlanarActionVarying(IfcStructuralPlanarAction):
    meta = {
        'namespace': 'ifc2x3'
    }

    VaryingAppliedLoadLocation = DynamicField()
    SubsequentAppliedLoads = DynamicField()
