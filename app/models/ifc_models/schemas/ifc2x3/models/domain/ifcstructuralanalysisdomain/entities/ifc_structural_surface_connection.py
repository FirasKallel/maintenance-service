from mongoengine import DynamicField
from .ifc_structural_connection import IfcStructuralConnection


class IfcStructuralSurfaceConnection(IfcStructuralConnection):
    meta = {
        'namespace': 'ifc2x3'
    }

