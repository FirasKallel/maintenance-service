from mongoengine import DynamicField
from .ifc_construction_resource import IfcConstructionResource


class IfcConstructionProductResource(IfcConstructionResource):
    meta = {
        'namespace': 'ifc2x3'
    }

