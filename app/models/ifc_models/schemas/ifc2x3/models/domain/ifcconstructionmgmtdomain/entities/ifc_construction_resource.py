from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_resource import IfcResource


class IfcConstructionResource(IfcResource):
    meta = {
        'namespace': 'ifc2x3'
    }

    ResourceIdentifier = DynamicField()
    ResourceGroup = DynamicField()
    ResourceConsumption = DynamicField()
    BaseQuantity = DynamicField()
