from mongoengine import DynamicField
from .ifc_construction_resource import IfcConstructionResource


class IfcSubContractResource(IfcConstructionResource):
    meta = {
        'namespace': 'ifc2x3'
    }

    SubContractor = DynamicField()
    JobDescription = DynamicField()
