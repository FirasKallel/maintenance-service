from mongoengine import DynamicField
from .ifc_construction_resource import IfcConstructionResource


class IfcConstructionEquipmentResource(IfcConstructionResource):
    meta = {
        'namespace': 'ifc2x3'
    }

