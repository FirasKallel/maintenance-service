from mongoengine import DynamicField
from .ifc_construction_resource import IfcConstructionResource


class IfcConstructionMaterialResource(IfcConstructionResource):
    meta = {
        'namespace': 'ifc2x3'
    }

    Suppliers = DynamicField()
    UsageRatio = DynamicField()
