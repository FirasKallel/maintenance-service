from mongoengine import DynamicField
from .ifc_construction_resource import IfcConstructionResource


class IfcLaborResource(IfcConstructionResource):
    meta = {
        'namespace': 'ifc2x3'
    }

    SkillSet = DynamicField()
