from mongoengine import DynamicField
from .ifc_building_element_component import IfcBuildingElementComponent


class IfcReinforcingElement(IfcBuildingElementComponent):
    meta = {
        'namespace': 'ifc2x3'
    }

    SteelGrade = DynamicField()
