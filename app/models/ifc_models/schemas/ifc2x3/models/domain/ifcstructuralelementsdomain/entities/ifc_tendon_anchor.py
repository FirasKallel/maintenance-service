from mongoengine import DynamicField
from .ifc_reinforcing_element import IfcReinforcingElement


class IfcTendonAnchor(IfcReinforcingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

