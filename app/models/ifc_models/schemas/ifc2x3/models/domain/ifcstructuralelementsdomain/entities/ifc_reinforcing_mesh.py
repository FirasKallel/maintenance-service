from mongoengine import DynamicField
from .ifc_reinforcing_element import IfcReinforcingElement


class IfcReinforcingMesh(IfcReinforcingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    MeshLength = DynamicField()
    MeshWidth = DynamicField()
    LongitudinalBarNominalDiameter = DynamicField()
    TransverseBarNominalDiameter = DynamicField()
    LongitudinalBarCrossSectionArea = DynamicField()
    TransverseBarCrossSectionArea = DynamicField()
    LongitudinalBarSpacing = DynamicField()
    TransverseBarSpacing = DynamicField()
