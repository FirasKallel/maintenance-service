from mongoengine import DynamicField
from .ifc_building_element_component import IfcBuildingElementComponent


class IfcBuildingElementPart(IfcBuildingElementComponent):
    meta = {
        'namespace': 'ifc2x3'
    }

