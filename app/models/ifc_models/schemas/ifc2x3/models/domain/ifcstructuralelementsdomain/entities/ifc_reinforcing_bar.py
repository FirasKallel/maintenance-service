from mongoengine import DynamicField
from .ifc_reinforcing_element import IfcReinforcingElement


class IfcReinforcingBar(IfcReinforcingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    NominalDiameter = DynamicField()
    CrossSectionArea = DynamicField()
    BarLength = DynamicField()
    BarRole = DynamicField()
    BarSurface = DynamicField()
