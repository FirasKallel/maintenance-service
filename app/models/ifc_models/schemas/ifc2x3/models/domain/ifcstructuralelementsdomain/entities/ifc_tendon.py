from mongoengine import DynamicField
from .ifc_reinforcing_element import IfcReinforcingElement


class IfcTendon(IfcReinforcingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
    NominalDiameter = DynamicField()
    CrossSectionArea = DynamicField()
    TensionForce = DynamicField()
    PreStress = DynamicField()
    FrictionCoefficient = DynamicField()
    AnchorageSlip = DynamicField()
    MinCurvatureRadius = DynamicField()
