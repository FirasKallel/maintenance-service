from mongoengine import DynamicField
from ....core.ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcReinforcementDefinitionProperties(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    DefinitionType = DynamicField()
    ReinforcementSectionDefinitions = DynamicField()
