from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_building_element import IfcBuildingElement


class IfcBuildingElementComponent(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

