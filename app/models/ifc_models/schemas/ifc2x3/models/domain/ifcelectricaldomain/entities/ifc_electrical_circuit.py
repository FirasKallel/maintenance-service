from mongoengine import DynamicField
from ....core.ifcproductextension.entities.ifc_system import IfcSystem


class IfcElectricalCircuit(IfcSystem):
    meta = {
        'namespace': 'ifc2x3'
    }

