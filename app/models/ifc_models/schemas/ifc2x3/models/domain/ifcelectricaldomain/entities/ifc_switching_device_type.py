from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_controller_type import IfcFlowControllerType


class IfcSwitchingDeviceType(IfcFlowControllerType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
