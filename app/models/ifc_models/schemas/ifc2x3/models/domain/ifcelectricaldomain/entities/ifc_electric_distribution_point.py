from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_controller import IfcFlowController


class IfcElectricDistributionPoint(IfcFlowController):
    meta = {
        'namespace': 'ifc2x3'
    }

    DistributionPointFunction = DynamicField()
    UserDefinedFunction = DynamicField()
