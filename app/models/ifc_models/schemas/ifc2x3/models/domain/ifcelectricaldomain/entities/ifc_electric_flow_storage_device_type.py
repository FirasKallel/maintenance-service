from mongoengine import DynamicField
from ....shared.ifcsharedbldgserviceelements.entities.ifc_flow_storage_device_type import IfcFlowStorageDeviceType


class IfcElectricFlowStorageDeviceType(IfcFlowStorageDeviceType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
