from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesConstraint(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    Intent = DynamicField()
    RelatingConstraint = DynamicField()
