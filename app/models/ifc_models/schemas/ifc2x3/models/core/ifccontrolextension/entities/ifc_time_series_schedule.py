from mongoengine import DynamicField
from ...ifckernel.entities.ifc_control import IfcControl


class IfcTimeSeriesSchedule(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    ApplicableDates = DynamicField()
    TimeSeriesScheduleType = DynamicField()
    TimeSeries = DynamicField()
