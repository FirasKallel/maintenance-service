from mongoengine import DynamicField
from ...ifckernel.entities.ifc_control import IfcControl


class IfcPerformanceHistory(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    LifeCyclePhase = DynamicField()
