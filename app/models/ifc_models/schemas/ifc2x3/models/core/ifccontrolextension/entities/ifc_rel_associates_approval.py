from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesApproval(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingApproval = DynamicField()
