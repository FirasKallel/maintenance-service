from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToProduct(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingProduct = DynamicField()
