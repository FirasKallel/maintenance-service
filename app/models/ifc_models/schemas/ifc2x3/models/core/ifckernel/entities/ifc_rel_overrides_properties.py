from mongoengine import DynamicField
from .ifc_rel_defines_by_properties import IfcRelDefinesByProperties


class IfcRelOverridesProperties(IfcRelDefinesByProperties):
    meta = {
        'namespace': 'ifc2x3'
    }

    OverridingProperties = DynamicField()
