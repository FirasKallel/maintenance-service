from mongoengine import DynamicField
from .ifc_property_set_definition import IfcPropertySetDefinition


class IfcPropertySet(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    HasProperties = DynamicField()
