from mongoengine import DynamicField
from .ifc_rel_defines import IfcRelDefines


class IfcRelDefinesByProperties(IfcRelDefines):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingPropertyDefinition = DynamicField()
