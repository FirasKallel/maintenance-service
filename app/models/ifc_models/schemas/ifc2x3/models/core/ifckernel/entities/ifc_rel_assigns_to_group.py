from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToGroup(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingGroup = DynamicField()
