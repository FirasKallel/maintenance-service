from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcProcess(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcProcess'
    }

