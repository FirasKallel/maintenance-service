from mongoengine import DynamicField
from .ifc_root import IfcRoot


class IfcObjectDefinition(IfcRoot):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcObjectDefinition'
    }

