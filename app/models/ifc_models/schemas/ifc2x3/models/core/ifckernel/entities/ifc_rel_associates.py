from mongoengine import DynamicField
from .ifc_relationship import IfcRelationship


class IfcRelAssociates(IfcRelationship):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelAssociates'
    }

    RelatedObjects = DynamicField()
