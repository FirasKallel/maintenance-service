from mongoengine import DynamicField, StringField
from .......common.ifc_base import IfcEntity


class IfcRoot(IfcEntity):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRoot',
        'indexes': [
            'GlobalId'
        ]
    }

    GlobalId = StringField()
    OwnerHistory = DynamicField()
    Name = DynamicField()
    Description = DynamicField()
