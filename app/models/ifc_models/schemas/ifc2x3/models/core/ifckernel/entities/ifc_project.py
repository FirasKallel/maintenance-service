from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcProject(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcProject'
    }

    LongName = DynamicField()
    Phase = DynamicField()
    RepresentationContexts = DynamicField()
    UnitsInContext = DynamicField()
