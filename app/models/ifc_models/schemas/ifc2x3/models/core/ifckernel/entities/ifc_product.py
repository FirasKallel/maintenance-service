from mongoengine import DynamicField
from .ifc_object import IfcObject
from .......common.ifc_base import IfcAbstractProduct


class IfcProduct(IfcObject, IfcAbstractProduct):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcProduct'
    }

    ObjectPlacement = DynamicField()
    Representation = DynamicField()
