from mongoengine import DynamicField
from .ifc_product import IfcProduct


class IfcProxy(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    ProxyType = DynamicField()
    Tag = DynamicField()
