from mongoengine import DynamicField
from .ifc_rel_connects import IfcRelConnects


class IfcRelSequence(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingProcess = DynamicField()
    RelatedProcess = DynamicField()
    TimeLag = DynamicField()
    SequenceType = DynamicField()
