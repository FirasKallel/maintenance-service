from mongoengine import DynamicField
from .ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesLibrary(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingLibrary = DynamicField()
