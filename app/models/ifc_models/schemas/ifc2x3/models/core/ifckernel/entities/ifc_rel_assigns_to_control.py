from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToControl(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingControl = DynamicField()
