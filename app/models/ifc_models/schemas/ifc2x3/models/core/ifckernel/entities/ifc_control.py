from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcControl(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcControl'
    }

