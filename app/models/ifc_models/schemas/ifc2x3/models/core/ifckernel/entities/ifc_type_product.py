from mongoengine import DynamicField
from .ifc_type_object import IfcTypeObject


class IfcTypeProduct(IfcTypeObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcTypeProduct'
    }

    RepresentationMaps = DynamicField()
    Tag = DynamicField()
