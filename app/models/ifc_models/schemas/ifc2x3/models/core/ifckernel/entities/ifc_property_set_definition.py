from mongoengine import DynamicField
from .ifc_property_definition import IfcPropertyDefinition


class IfcPropertySetDefinition(IfcPropertyDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

