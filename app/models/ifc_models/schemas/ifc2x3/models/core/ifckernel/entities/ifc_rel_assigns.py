from mongoengine import DynamicField
from .ifc_relationship import IfcRelationship


class IfcRelAssigns(IfcRelationship):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelAssigns'
    }

    RelatedObjects = DynamicField()
    RelatedObjectsType = DynamicField()
