from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcGroup(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcGroup'
    }

