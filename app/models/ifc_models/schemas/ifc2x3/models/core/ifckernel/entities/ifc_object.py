from mongoengine import DynamicField
from .ifc_object_definition import IfcObjectDefinition


class IfcObject(IfcObjectDefinition):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcObject'
    }

    ObjectType = DynamicField()
