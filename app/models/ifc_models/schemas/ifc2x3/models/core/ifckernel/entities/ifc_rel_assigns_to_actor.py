from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToActor(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingActor = DynamicField()
    ActingRole = DynamicField()
