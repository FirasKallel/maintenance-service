from mongoengine import DynamicField
from .ifc_relationship import IfcRelationship


class IfcRelDefines(IfcRelationship):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelDefines'
    }

    RelatedObjects = DynamicField()
