from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToResource(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingResource = DynamicField()
