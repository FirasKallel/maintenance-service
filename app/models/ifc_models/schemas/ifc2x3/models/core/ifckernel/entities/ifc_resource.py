from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcResource(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcResource'
    }

