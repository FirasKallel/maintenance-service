from mongoengine import DynamicField
from .ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesDocument(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingDocument = DynamicField()
