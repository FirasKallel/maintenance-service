from mongoengine import DynamicField
from .ifc_object import IfcObject


class IfcActor(IfcObject):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcActor'
    }

    TheActor = DynamicField()
