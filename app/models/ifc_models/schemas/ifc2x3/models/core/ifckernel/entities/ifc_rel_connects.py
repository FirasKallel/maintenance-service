from mongoengine import DynamicField
from .ifc_relationship import IfcRelationship


class IfcRelConnects(IfcRelationship):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelConnects'
    }

