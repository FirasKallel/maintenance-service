from mongoengine import DynamicField
from .ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesClassification(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingClassification = DynamicField()
