from mongoengine import DynamicField
from .ifc_rel_assigns import IfcRelAssigns


class IfcRelAssignsToProcess(IfcRelAssigns):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingProcess = DynamicField()
    QuantityInProcess = DynamicField()
