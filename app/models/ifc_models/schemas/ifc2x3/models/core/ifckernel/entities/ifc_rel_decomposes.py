from mongoengine import DynamicField
from .ifc_relationship import IfcRelationship


class IfcRelDecomposes(IfcRelationship):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelDecomposes'
    }

    RelatingObject = DynamicField()
    RelatedObjects = DynamicField()
