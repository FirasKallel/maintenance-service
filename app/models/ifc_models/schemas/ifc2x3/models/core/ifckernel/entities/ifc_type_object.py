from mongoengine import DynamicField
from .ifc_object_definition import IfcObjectDefinition


class IfcTypeObject(IfcObjectDefinition):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcTypeObject'
    }

    ApplicableOccurrence = DynamicField()
    HasPropertySets = DynamicField()
