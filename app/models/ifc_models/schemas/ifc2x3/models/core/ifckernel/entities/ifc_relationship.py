from mongoengine import DynamicField
from .ifc_root import IfcRoot


class IfcRelationship(IfcRoot):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcRelationship'
    }

