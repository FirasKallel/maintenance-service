from mongoengine import DynamicField
from .ifc_root import IfcRoot


class IfcPropertyDefinition(IfcRoot):
    meta = {
        'namespace': 'ifc2x3',
        'collection': 'IfcPropertyDefinition'
    }

