from mongoengine import DynamicField
from .ifc_rel_defines import IfcRelDefines


class IfcRelDefinesByType(IfcRelDefines):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingType = DynamicField()
