from mongoengine import DynamicField
from .ifc_rel_decomposes import IfcRelDecomposes


class IfcRelAggregates(IfcRelDecomposes):
    meta = {
        'namespace': 'ifc2x3'
    }

