from mongoengine import DynamicField
from ...ifckernel.entities.ifc_control import IfcControl


class IfcWorkControl(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    Identifier = DynamicField()
    CreationDate = DynamicField()
    Creators = DynamicField()
    Purpose = DynamicField()
    Duration = DynamicField()
    TotalFloat = DynamicField()
    StartTime = DynamicField()
    FinishTime = DynamicField()
    WorkControlType = DynamicField()
    UserDefinedControlType = DynamicField()
