from mongoengine import DynamicField
from .ifc_work_control import IfcWorkControl


class IfcWorkPlan(IfcWorkControl):
    meta = {
        'namespace': 'ifc2x3'
    }

