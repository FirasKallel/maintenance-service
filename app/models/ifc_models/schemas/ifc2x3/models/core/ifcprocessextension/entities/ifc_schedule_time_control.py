from mongoengine import DynamicField
from ...ifckernel.entities.ifc_control import IfcControl


class IfcScheduleTimeControl(IfcControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    ActualStart = DynamicField()
    EarlyStart = DynamicField()
    LateStart = DynamicField()
    ScheduleStart = DynamicField()
    ActualFinish = DynamicField()
    EarlyFinish = DynamicField()
    LateFinish = DynamicField()
    ScheduleFinish = DynamicField()
    ScheduleDuration = DynamicField()
    ActualDuration = DynamicField()
    RemainingTime = DynamicField()
    FreeFloat = DynamicField()
    TotalFloat = DynamicField()
    IsCritical = DynamicField()
    StatusTime = DynamicField()
    StartFloat = DynamicField()
    FinishFloat = DynamicField()
    Completion = DynamicField()
