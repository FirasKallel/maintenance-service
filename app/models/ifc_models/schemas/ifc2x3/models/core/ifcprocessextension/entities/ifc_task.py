from mongoengine import DynamicField
from ...ifckernel.entities.ifc_process import IfcProcess


class IfcTask(IfcProcess):
    meta = {
        'namespace': 'ifc2x3'
    }

    TaskId = DynamicField()
    Status = DynamicField()
    WorkMethod = DynamicField()
    IsMilestone = DynamicField()
    Priority = DynamicField()
