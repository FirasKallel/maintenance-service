from mongoengine import DynamicField
from ...ifckernel.entities.ifc_process import IfcProcess


class IfcProcedure(IfcProcess):
    meta = {
        'namespace': 'ifc2x3'
    }

    ProcedureID = DynamicField()
    ProcedureType = DynamicField()
    UserDefinedProcedureType = DynamicField()
