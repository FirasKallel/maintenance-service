from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_assigns_to_control import IfcRelAssignsToControl


class IfcRelAssignsTasks(IfcRelAssignsToControl):
    meta = {
        'namespace': 'ifc2x3'
    }

    TimeForTask = DynamicField()
