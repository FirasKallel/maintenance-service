from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcTransportElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    OperationType = DynamicField()
    CapacityByWeight = DynamicField()
    CapacityByNumber = DynamicField()
