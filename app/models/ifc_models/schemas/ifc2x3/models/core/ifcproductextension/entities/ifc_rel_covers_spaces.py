from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelCoversSpaces(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatedSpace = DynamicField()
    RelatedCoverings = DynamicField()
