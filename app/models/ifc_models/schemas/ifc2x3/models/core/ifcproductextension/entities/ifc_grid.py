from mongoengine import DynamicField
from ...ifckernel.entities.ifc_product import IfcProduct


class IfcGrid(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    UAxes = DynamicField()
    VAxes = DynamicField()
    WAxes = DynamicField()
