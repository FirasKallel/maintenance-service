from mongoengine import DynamicField
from ...ifckernel.entities.ifc_product import IfcProduct


class IfcSpatialStructureElement(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    LongName = DynamicField()
    CompositionType = DynamicField()
