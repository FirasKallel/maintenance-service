from mongoengine import DynamicField
from .ifc_building_element import IfcBuildingElement


class IfcCovering(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
