from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcVirtualElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

