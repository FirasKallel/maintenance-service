from mongoengine import DynamicField
from .ifc_building_element_type import IfcBuildingElementType


class IfcCoveringType(IfcBuildingElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
