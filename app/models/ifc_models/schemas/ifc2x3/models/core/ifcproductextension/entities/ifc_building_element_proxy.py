from mongoengine import DynamicField
from .ifc_building_element import IfcBuildingElement


class IfcBuildingElementProxy(IfcBuildingElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    CompositionType = DynamicField()
