from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelSpaceBoundary(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingSpace = DynamicField()
    RelatedBuildingElement = DynamicField()
    ConnectionGeometry = DynamicField()
    PhysicalOrVirtualBoundary = DynamicField()
    InternalOrExternalBoundary = DynamicField()
