from mongoengine import DynamicField
from .ifc_spatial_structure_element import IfcSpatialStructureElement


class IfcBuildingStorey(IfcSpatialStructureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    Elevation = DynamicField()
