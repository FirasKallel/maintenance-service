from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelReferencedInSpatialStructure(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatedElements = DynamicField()
    RelatingStructure = DynamicField()
