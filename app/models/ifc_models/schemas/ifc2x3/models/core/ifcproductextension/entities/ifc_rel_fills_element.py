from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelFillsElement(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingOpeningElement = DynamicField()
    RelatedBuildingElement = DynamicField()
