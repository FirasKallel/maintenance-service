from mongoengine import DynamicField
from ...ifckernel.entities.ifc_property_set_definition import IfcPropertySetDefinition


class IfcElementQuantity(IfcPropertySetDefinition):
    meta = {
        'namespace': 'ifc2x3'
    }

    MethodOfMeasurement = DynamicField()
    Quantities = DynamicField()
