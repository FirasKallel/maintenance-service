from mongoengine import DynamicField
from .ifc_spatial_structure_element_type import IfcSpatialStructureElementType


class IfcSpaceType(IfcSpatialStructureElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
