from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcFeatureElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

