from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelConnectsElements(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    ConnectionGeometry = DynamicField()
    RelatingElement = DynamicField()
    RelatedElement = DynamicField()
