from mongoengine import DynamicField
from .ifc_element_type import IfcElementType


class IfcTransportElementType(IfcElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

    PredefinedType = DynamicField()
