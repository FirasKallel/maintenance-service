from mongoengine import DynamicField
from ...ifckernel.entities.ifc_product import IfcProduct


class IfcElement(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    Tag = DynamicField()
