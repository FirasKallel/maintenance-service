from mongoengine import DynamicField
from .ifc_feature_element import IfcFeatureElement


class IfcFeatureElementSubtraction(IfcFeatureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

