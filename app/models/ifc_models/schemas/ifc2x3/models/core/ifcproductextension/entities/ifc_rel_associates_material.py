from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_associates import IfcRelAssociates


class IfcRelAssociatesMaterial(IfcRelAssociates):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingMaterial = DynamicField()
