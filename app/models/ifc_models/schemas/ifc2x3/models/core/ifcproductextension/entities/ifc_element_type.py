from mongoengine import DynamicField
from ...ifckernel.entities.ifc_type_product import IfcTypeProduct


class IfcElementType(IfcTypeProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

    ElementType = DynamicField()
