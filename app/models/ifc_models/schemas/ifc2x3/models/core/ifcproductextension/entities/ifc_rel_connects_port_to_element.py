from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelConnectsPortToElement(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingPort = DynamicField()
    RelatedElement = DynamicField()
