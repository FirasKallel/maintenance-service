from mongoengine import DynamicField
from .ifc_spatial_structure_element import IfcSpatialStructureElement


class IfcSite(IfcSpatialStructureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    RefLatitude = DynamicField()
    RefLongitude = DynamicField()
    RefElevation = DynamicField()
    LandTitleNumber = DynamicField()
    SiteAddress = DynamicField()
