from mongoengine import DynamicField
from .ifc_spatial_structure_element import IfcSpatialStructureElement


class IfcSpace(IfcSpatialStructureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    InteriorOrExteriorSpace = DynamicField()
    ElevationWithFlooring = DynamicField()
