from mongoengine import DynamicField
from .ifc_spatial_structure_element import IfcSpatialStructureElement


class IfcBuilding(IfcSpatialStructureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    ElevationOfRefHeight = DynamicField()
    ElevationOfTerrain = DynamicField()
    BuildingAddress = DynamicField()
