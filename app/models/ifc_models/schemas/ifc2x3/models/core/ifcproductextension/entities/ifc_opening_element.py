from mongoengine import DynamicField
from .ifc_feature_element_subtraction import IfcFeatureElementSubtraction


class IfcOpeningElement(IfcFeatureElementSubtraction):
    meta = {
        'namespace': 'ifc2x3'
    }

