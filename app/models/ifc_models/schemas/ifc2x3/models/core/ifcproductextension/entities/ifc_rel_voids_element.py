from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelVoidsElement(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingBuildingElement = DynamicField()
    RelatedOpeningElement = DynamicField()
