from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelProjectsElement(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingElement = DynamicField()
    RelatedFeatureElement = DynamicField()
