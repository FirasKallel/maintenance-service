from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcEquipmentElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

