from mongoengine import DynamicField
from ...ifckernel.entities.ifc_rel_connects import IfcRelConnects


class IfcRelServicesBuildings(IfcRelConnects):
    meta = {
        'namespace': 'ifc2x3'
    }

    RelatingSystem = DynamicField()
    RelatedBuildings = DynamicField()
