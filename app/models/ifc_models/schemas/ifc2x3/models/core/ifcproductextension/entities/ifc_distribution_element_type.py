from mongoengine import DynamicField
from .ifc_element_type import IfcElementType


class IfcDistributionElementType(IfcElementType):
    meta = {
        'namespace': 'ifc2x3'
    }

