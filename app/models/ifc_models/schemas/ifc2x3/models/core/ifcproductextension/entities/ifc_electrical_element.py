from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcElectricalElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

