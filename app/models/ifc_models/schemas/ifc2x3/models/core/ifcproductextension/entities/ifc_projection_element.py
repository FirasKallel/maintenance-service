from mongoengine import DynamicField
from .ifc_feature_element_addition import IfcFeatureElementAddition


class IfcProjectionElement(IfcFeatureElementAddition):
    meta = {
        'namespace': 'ifc2x3'
    }

