from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcFurnishingElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

