from mongoengine import DynamicField
from ...ifckernel.entities.ifc_product import IfcProduct


class IfcPort(IfcProduct):
    meta = {
        'namespace': 'ifc2x3'
    }

