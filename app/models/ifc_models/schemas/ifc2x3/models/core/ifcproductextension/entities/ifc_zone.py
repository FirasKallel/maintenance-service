from mongoengine import DynamicField
from ...ifckernel.entities.ifc_group import IfcGroup


class IfcZone(IfcGroup):
    meta = {
        'namespace': 'ifc2x3'
    }

