from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcBuildingElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

