from mongoengine import DynamicField
from .ifc_feature_element import IfcFeatureElement


class IfcFeatureElementAddition(IfcFeatureElement):
    meta = {
        'namespace': 'ifc2x3'
    }

