from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcElementAssembly(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

    AssemblyPlace = DynamicField()
    PredefinedType = DynamicField()
