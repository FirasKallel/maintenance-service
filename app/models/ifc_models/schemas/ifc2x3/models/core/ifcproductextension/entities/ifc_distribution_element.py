from mongoengine import DynamicField
from .ifc_element import IfcElement


class IfcDistributionElement(IfcElement):
    meta = {
        'namespace': 'ifc2x3'
    }

