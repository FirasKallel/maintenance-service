from mongoengine import DynamicField
from .ifc_rel_connects_elements import IfcRelConnectsElements


class IfcRelConnectsWithRealizingElements(IfcRelConnectsElements):
    meta = {
        'namespace': 'ifc2x3'
    }

    RealizingElements = DynamicField()
    ConnectionType = DynamicField()
