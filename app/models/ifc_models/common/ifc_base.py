from mongoengine import Document, ReferenceField, IntField, DynamicField, EmbeddedDocument, FileField, StringField, \
    EmbeddedDocumentField, ListField, FloatField, GenericReferenceField, ObjectIdField

from .utils import Singleton


class Reference(EmbeddedDocument):
    """
    Refers to another entity by its index
    """
    reference = GenericReferenceField()
    index = DynamicField()


class Project(Document):
    header = DynamicField()

    def __init__(self, *args, **values):
        super().__init__(*args, **values)

    @property
    def entities(self):
        return self.__entities

    @entities.setter
    def entities(self, entities):
        self.__entities = entities

        for k, e in self.__entities.items():
            e.project = self
            e.expressRef = k

    def save(
            self,
            force_insert=False,
            validate=True,
            clean=True,
            write_concern=None,
            cascade=None,
            cascade_kwargs=None,
            _refs=None,
            save_condition=None,
            signal_kwargs=None,
            **kwargs
    ):
        super().save(force_insert, validate, clean, write_concern, cascade, cascade_kwargs, _refs, save_condition,
                     signal_kwargs, **kwargs)

        level_dependency = []

        def process_iterator(a):
            level_nb = -1
            for field in a:
                if isinstance(field, Reference) and field.reference:
                    level_nb = max(level_nb, dependency(field.reference))
                elif isinstance(field, (dict, list, tuple)):
                    level_nb = max(level_nb, process_iterator(field))
            return level_nb

        def dependency(e):
            if e.done:
                return e.level

            level_nb = -1
            for field in e._data.values():
                if isinstance(field, Reference) and field.reference:
                    level_nb = max(level_nb, dependency(field.reference))
                elif isinstance(field, (dict, list, tuple)):
                    level_nb = max(level_nb, process_iterator(field))

            e.done = True
            e.level = level_nb + 1

            if e.level == len(level_dependency):
                level_dependency.append({})
            _level = level_dependency[e.level]
            _entities = _level.setdefault(e.__class__, [])
            _entities.append(e)

            return e.level

        for e in self.__entities.values():
            e.done = False
        for e in self.__entities.values():
            dependency(e)

        chunk_size = 1000
        for level in level_dependency:
            for _class_, entities in level.items():
                for x in range(0, len(entities), chunk_size):
                    _class_.objects.insert(entities[x:x + chunk_size])
                entities.clear()


class IfcEntity(Document):
    """
        Generic IFC entity, only for subclassing from it
    """
    project = ReferenceField(Project)
    expressRef = IntField()
    rtype = DynamicField()
    meta = {
        'abstract': True,
        'allow_inheritance': True
    }


class IfcScalarValue(EmbeddedDocument):
    meta = {
        'allow_inheritance': True
    }
    rtype = DynamicField()
    value = DynamicField()


class REAL(IfcScalarValue):
    pass


class BINARY(IfcScalarValue):
    pass


class INTEGER(IfcScalarValue):
    pass


class NUMBER(IfcScalarValue):
    pass


class STRING(IfcScalarValue):
    pass


class BOOLEAN(IfcScalarValue):
    pass


class LOGICAL(BOOLEAN):
    pass


class Unset(EmbeddedDocument, metaclass=Singleton):
    """
        Marked with '$' it states that this value is not set
        """
    pass


class Omitted(EmbeddedDocument, metaclass=Singleton):
    """
    Marked with '*' it states that some supertype had defined that attribute, but in the subtype it is a derived
    (calculated) value, so it no longer makes sense to explicitely assign value to it.
    """
    pass


class EnumValue(EmbeddedDocument):
    """
    Item from some set of enumerated values.
    """
    value = DynamicField()


class STEPHeader(EmbeddedDocument):
    fields = DynamicField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields = {}

    def add(self, e):
        self.fields[e.rtype] = e


class GeometryInstance(EmbeddedDocument):
    meta = {
        'strict': False,
    }
    materialId = ObjectIdField()
    meshId = ObjectIdField()
    transformation = ListField(FloatField())


class IfcAbstractProduct:
    geometryInstance = EmbeddedDocumentField(GeometryInstance)
    ifc_project = DynamicField()
    ifc_site = DynamicField()
    ifc_building = DynamicField()
    ifc_building_storey = DynamicField()
    ifc_systems = DynamicField()
    ifc_zones = DynamicField()
