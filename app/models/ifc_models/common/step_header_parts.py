from mongoengine import EmbeddedDocument, DynamicField
from .ifc_base import STRING


class time_stamp_text(STRING):
    pass


class schema_name(STRING):
    pass


class context_name(STRING):
    pass


class exchange_structure_identifier(STRING):
    pass


class section_name(exchange_structure_identifier):
    pass


class language_name(exchange_structure_identifier):
    pass


class SECTION_CONTEXT(EmbeddedDocument):
    rtype = DynamicField()
    section = DynamicField()
    context_identifiers = DynamicField()


class SCHEMA_POPULATION(EmbeddedDocument):
    rtype = DynamicField()
    external_file_identifications = DynamicField()


class FILE_NAME(EmbeddedDocument):
    rtype = DynamicField()
    name = DynamicField()
    time_stamp = DynamicField()
    author = DynamicField()
    organization = DynamicField()
    preprocessor_version = DynamicField()
    originating_system = DynamicField()
    authorization = DynamicField()


class FILE_POPULATION(EmbeddedDocument):
    rtype = DynamicField()
    governing_schema = DynamicField()
    determination_method = DynamicField()
    governed_sections = DynamicField()


class FILE_DESCRIPTION(EmbeddedDocument):
    rtype = DynamicField()
    description = DynamicField()
    implementation_level = DynamicField()


class SECTION_LANGUAGE(EmbeddedDocument):
    rtype = DynamicField()
    section = DynamicField()
    default_language = DynamicField()


class FILE_SCHEMA(EmbeddedDocument):
    rtype = DynamicField()
    schema_identifiers = DynamicField()

# vim: set sw=4 ts=4 et:
