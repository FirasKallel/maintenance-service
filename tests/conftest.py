from fastapi.testclient import TestClient
from typing import Generator
import pytest


@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c
        